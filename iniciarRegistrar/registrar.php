<?php
  include('registrarPHP.php');
  session_start();
  require '../idioma/requirelanguage.php'; // idioma
?>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Bike Tour Barcelona</title>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="css/css.css">
</head>
<body style="background-color: #DDE3F7;">
  <br>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="card">
          <article class="card-body">
            <form method="post" action="registrar.php">
              <center><h3><?php echo $registrar; ?></h3></center>
              <br>
              <div class="form-row">
                <div class="col form-group">
                  <label><?php echo $nom; ?></label>
                  <input type="text" name="nom" class="form-control" placeholder="<?php echo $nom; ?>..." required>
                </div>
                <div class="col form-group">
                  <label><?php echo $cognoms; ?></label>
                  <input type="text" name="cognoms" class="form-control" placeholder="<?php echo $cognoms; ?>..." required>
                </div>
              </div>
              <div class="form-row">
                <div class="col form-group">
                  <label><?php echo $email; ?></label>
                  <input type="email" name="email" class="form-control" placeholder="<?php echo $email; ?>..." required>
                </div>
                <div class="col form-group">
                  <label><?php echo $sexe; ?></label>
                  <select class="form-control" name="homeDona" id="exampleFormControlSelect">
                    <option>home</option>
                    <option>dona</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label><?php echo $contrasenya; ?></label>
                <input class="form-control" name="contrasenya" type="password" placeholder="<?php echo $contrasenya; ?>..." required>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label><?php echo $ciutat; ?></label>
                  <input type="text" name="ciutat" placeholder="<?php echo $ciutat; ?>..." class="form-control" required>
                </div>
                <div class="form-group col-md-6">
                  <label><?php echo $pais; ?></label>
                  <select id="inputState" class="form-control" name="pais" required>
                    <option>Albania</option>
                    <option>Algeria</option>
                    <option>Andorra</option>
                    <option>Argentina</option>
                    <option>Australia</option>
                    <option>Austria</option>
                    <option>Belgium</option>
                    <option>Brazil</option>
                    <option>Chile</option>
                    <option>China</option>
                    <option>Estonia</option>
                    <option>France</option>
                    <option>Georgia</option>
                    <option>Germany</option>
                    <option>Great Britain</option>
                    <option>Greece</option>
                    <option>Hungary</option>
                    <option>Israel</option>
                    <option>Italy</option>
                    <option>Japan</option>
                    <option>Latvia</option>
                    <option>Morocco</option>
                    <option>Portugal</option>
                    <option>Russia</option>
                    <option selected>Spain</option>
                    <option>Turkey</option>
                    <option>United Arab Emirates</option>
                    <option>Other</option>
                  </select>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label><?php echo $DNI; ?></label>
                  <input type="text" name="dni" placeholder="<?php echo $DNI; ?>..." class="form-control" required>
                </div>
                <div class="form-group col-md-6">
                  <label><?php echo $mobil; ?></label>
                  <input type="number" name="mobil" placeholder="<?php echo $mobil; ?>..." class="form-control" required>
                </div>
              </div>
              <div class="form-row">
                <div class="col form-group">
                  <label><?php echo $naixement; ?></label>
                  <input type="date" class="form-control" required>
                </div>
                <div class="col form-group">
                  <label><?php echo $targeta; ?></label>
                  <input type="text" name="numeroTargeta" class="form-control" placeholder="<?php echo $targeta; ?>..." required>
                </div>
              </div>
              <br>
              <div class="form-group">
                <button type="submit" name="submit" class="btn btn-primary" style="float: right;"> <?php echo $registrar; ?> </button>
              </div>
              <?php
                if (isset($_POST['submit'])) {
                  echo "<script language='JavaScript'>location.href = 'usuariJaRegistrat.php' </script>";
                }
              ?>
              <br>
            </form>
          </article>
          <div class="border-top card-body text-center"><a href="iniciar.php" target="_blank"><?php echo $iniciarSessió; ?></a></div>
          <div class="border-top card-body text-center"><a href="../index.php" target="_blank"><?php echo $tornarPagina; ?></a></div>
        </div>
        <br>
      </div>
    </div>
  </div>
</body>
</html>
