<?php
	session_start();

	if (!isset($_SESSION['usuario'])) {
		header('Location: ../loginAdmin.php');
	}

	if (isset($_POST['button'])) {
		session_destroy();
		header('Location: ../../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Bike Tour Barcelona</title>
  <meta charset="utf-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body id="top" style="margin-left: 25px; margin-top: 10px; margin-right: 25px">
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
	    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
				<li class="nav-item active">
	        <a class="nav-link" href="principal.php">Inici</a>
	      </li>
				<li class="nav-item active">
	        <a class="nav-link" href="consultes.php">Consultes</a>
	      </li>
				<li class="nav-item active">
					<a class="nav-link" href="updateBicis.php">Update bicicleta</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="reserves.php">Reserves</a>
				</li>
				<li class="nav-item active">
					<b><u><a class="nav-link">ADMINISTRADORS</a></b></u>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="blog.php">Blog</a>
				</li>
	    </ul>
			<span class="navbar-text">
				<form method="POST">
					<button type="submit" name="button" id="buttonMarxar" class="btn btn-danger">Log out</button>
				</form>
			</span>
	  </div>
	</nav>

	<br><br>

	<center>
		<h3>Els nostres administradors</h3>
	</center>

  <br>

  <?php
    $usuaris = "root";
    $contrasenyes = "";
    $servidors = "localhost";
    $basededadess = "biketourbarcelona";

    $conexions = mysqli_connect($servidors, $usuaris, "") or die ("No se ha conectado");
    $dbs = mysqli_select_db($conexions, $basededadess) or die ("No se ha conectado");
    $consultas = "SELECT * FROM administrador";
    $resultados = mysqli_query($conexions, $consultas) or die ("No se ha hecho la consulta");

    echo "<table borde='2' style='width:100%' bgcolor='#E3F0FD'>";
    echo "<tr>";
    echo "<th><u>Email</u></th>";
    echo "</tr>";

    while ($columnas = mysqli_fetch_array($resultados)){
      echo "<tr>";
      echo "<td>" . $columnas['nom'] . "</td>";
      echo "</tr>";
    }

    echo "</table>";
  ?>

  <br><hr><br>

  <center>
		<h3>Crear treballador</h3>
	</center>

	<form action="administradors.php" style="margin-right: 70%" method="post">
		<div class="form-group">
			<label>ID:</label>
			<input type="number" class="form-control" name="id">
		</div>
		<div class="form-group">
			<label>Compte bancari:</label>
			<input type="text" class="form-control" name="compte">
		</div>
		<div class="form-group">
			<label>Seguretat social:</label>
			<input type="text" class="form-control" name="seguretatsocial">
		</div>
		<div class="form-group">
			<label>Salari:</label>
			<input type="number" class="form-control" name="salari">
		</div>
		<button type="submit" name="creacio" class="btn btn-primary">Crear</button>
	</form>

	<?php
		if (isset($_POST["creacio"])) {
			$conn = new mysqli("localhost", "root", "", "biketourbarcelona");

			$idTreballador = $_POST['id'];
			$compteBancari = md5($_POST["compte"]);
			$seguretatsocial = md5($_POST["seguretatsocial"]);
			$salari = $_POST['salari'];

			$sql = "INSERT INTO treballador (id_treballador, compteBancari, seguretatSocial, salari)
				VALUES ($idTreballador, '$compteBancari', '$seguretatsocial', $salari)";

			if ($conn->query($sql) === TRUE) {
			    echo "Afegit treballador";
			} else {
				echo "Error";
			}

			$conn->close();
		}
	?>

	<br><hr><br>

	<footer class="page-footer font-small">
	  <div class="footer-copyright text-center py-3">Pàgina administrador:
	    <a href="http://localhost:8080/M12-Projecte-Oriol_Mainou/Projecte-Web/"> Bike Tour Barcelona</a>
	  </div>
	</footer>

</body>
</html>
