<?php
	session_start();
	require '../idioma/requirelanguage.php'; // idioma

	if (isset($_SESSION['usuario'])) {
		header('Location: prueba/principal.php');
	}
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Bike Tour Barcelona</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
	<div class="divPrincipal">
		<div class="clasePrimera">
			<form class="formulari" method="POST" action="prueba/valida.php">
				<span class="formulari-titol"><?php echo $iniciarSessió; ?></span>
				<div class="dades">
					<label class="labelDades" for="email"><?php echo "$email"; ?></label>
					<input id="email" title="<?php echo "$email"; ?>" placeholder="<?php echo "$email"; ?>..." class="datos" type="text" name="usuario" required>
				</div>
				<div class="dades">
					<label class="labelDades" for="password"><?php echo $contrasenya; ?></label>
					<input id="password" title="<?php echo "$contrasenya"; ?>" class="datos" type="password" name="clave" placeholder="<?php echo $contrasenya; ?>..." required>
				</div>
				<div class="formulariBoton">
					<button class="formulariBtn" title="<?php echo $entrar; ?>" name="formulariBoton"><?php echo $entrar; ?></button>
				</div>
				<br> <hr> <br>
				<div>
					<center>
						<p style="font-size: 14px;"><?php echo $oblidacio; ?> <a href="oblidar_contrasenya/form_oblidar.php" target="_blank" title="<?php echo $entrar; ?>"><?php echo $aqui; ?></a></p>
					</center>
				</div>
			</form>
			<div class="imatgeFons" style="background-image: url('img/ciutat.jpg');"></div>
		</div>
	</div>
</body>
</html>
