<?php
	session_start();
	require '../../idioma/requirelanguage.php'; // idioma

	if (!isset($_SESSION['usuario'])) {
		header('Location: ../iniciar.php');
	}

	if (isset($_POST['button'])) {
		session_destroy();
		header('Location: ../../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Bike Tour Barcelona</title>
  <meta charset="utf-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script LANGUAGE="JavaScript">
		function abreSitio(){
			var web = document.form1.sitio.options[document.form1.sitio.selectedIndex].value;
			window.open(web);
		}
	</script>
</head>
<body id="top" style="margin-left: 25px; margin-top: 10px; margin-right: 25px">
	<?php
		if (isset($_GET['err'])) {
			echo "<script>alert('$dataMal')</script>";
		}
	?>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
	    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
				<li class="nav-item active">
					<form method="POST">
						<button type="submit" name="button" id="buttonMarxar" class="btn btn-info"><?php echo $marxar; ?></button>
					</form>
	      </li>
				<li style="margin-left: 3px" class="nav-item active">
					<form method="POST" >
						<button type="submit" id="buttonEliminarUsuari" name="buttonEliminarUsuari" class="btn btn-danger"><?php echo $eliminarCompte; ?></button>
					</form>
					<?php
						if (isset($_POST['buttonEliminarUsuari'])) {
							$usuari2 = "root";
							$contrasenya2 = "";
							$servidor2 = "localhost";
							$basededades2 = "biketourbarcelona";

							$conexion2 = mysqli_connect($servidor2, $usuari2, "") or die ("No se ha conectado");
							$db2 = mysqli_select_db($conexion2, $basededades2) or die ("No se ha conectado");
							$consulta2 = "DELETE FROM persona WHERE email = " . "'". $_SESSION['email']. "'";
							$resultado2 = mysqli_query($conexion2, $consulta2) or die ("No se ha hecho la consulta");
							mysqli_close($conexion2);

							session_destroy();
							header('Location: ../iniciar.php');
						}
					?>
	      </li>
	    </ul>
			<span class="navbar-text">
				<form name="form1" method="post">
					<select id="idiomas" name="sitio" onChange="javascript:abreSitio()">
            <option><?php echo $idioma ?></option>
            <option value="../../idioma/changelanguage.php?language=ca">- <?php echo $catala ?></a></option>
            <option value="../../idioma/changelanguage.php?language=es">- <?php echo $castella ?></a></option>
            <option value="../../idioma/changelanguage.php?language=en">- <?php echo $angles ?></a></option>
          </select>
        </form>
			</span>
	  </div>
	</nav>

	<br><br>

	<?php
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "biketourbarcelona";

		$conn = mysqli_connect($servername, $username, $password, $dbname);
		$idUser = $_SESSION['usuario'];
		$sql = "SELECT * FROM persona WHERE email = '$idUser'";
		$result = mysqli_query($conn, $sql);
	?>

	<center>
		<div class="card" style="width: 18rem;">
		  <?php
				if (mysqli_num_rows($result) == true) {
					$row = mysqli_fetch_assoc($result);
					$sexeHome = $row["sexe"];
					if ($sexeHome == "home") {
						?>
							<img src="../../images/img_avatar.png" title="<?php echo $benvingut . " " . $_SESSION['nom'] . "!";?>" style="width:100%">
						<?php
					} else {
						?>
							<img src="../../images/img_avatar_mujer.png" title="<?php echo $benvinguda . " " . $_SESSION['nom'] . "!";?>" style="width:90%; margin-top: 5px;">
						<?php
					}
				}
			?>
		  <div class="card-body">
		    <h3 class="card-title"><?php echo $_SESSION['nom'] . " " . $_SESSION['cognoms']; ?></h3>
		    <p class="card-text"><?php echo $_SESSION['email']; ?></p>
				<p class="card-text"><?php echo $_SESSION['mobil']; ?></p>
		  </div>
		</div>
	</center>

	<br><br>

	<table class="table">
	  <thead>
	    <tr>
				<th><?php echo "ID"; ?></th>
				<th><?php echo $bicisReservades; ?></th>
				<th><?php echo $dataReserva; ?></th>
				<th><?php echo $ruta; ?></th>
	    </tr>
	  </thead>
	  <tbody>
	    <tr>
				<td>
					<?php
						$correoReservadesBicis = $_SESSION['email'];
						$sqls = "SELECT id FROM bicicletes WHERE emailUsuariRegistrat = '$correoReservadesBicis'";
						$result = mysqli_query($conn, $sqls);

						if (mysqli_num_rows($result) > 0) {
							while($row = mysqli_fetch_assoc($result)) {
								echo $row["id"] . "<br><br>";
							}
						}
					?>
				</td>
				<td>
					<?php
						$correoReservadesBicis = $_SESSION['email'];
						$sqls = "SELECT reservades FROM bicicletes WHERE emailUsuariRegistrat = '$correoReservadesBicis'";
						$result = mysqli_query($conn, $sqls);

						if (mysqli_num_rows($result) > 0) {
							while($row = mysqli_fetch_assoc($result)) {
								echo "$bicisJaReservades " . $row["reservades"] . "<br><br>";
							}
						}
					?>
				</td>
				<td>
					<?php
						$correoReservadesBicis = $_SESSION['email'];
						$sqls = "SELECT diaHoraReserva FROM bicicletes WHERE emailUsuariRegistrat = '$correoReservadesBicis'";
						$result = mysqli_query($conn, $sqls);

						if (mysqli_num_rows($result)) {
							while($row = mysqli_fetch_assoc($result)) {
								echo $row["diaHoraReserva"];
								echo "<br><br>";
							}
						}
					?>
				</td>
				<td>
					<?php
						$correoReservadesBicis = $_SESSION['email'];
						$sqls = "SELECT ruta FROM bicicletes WHERE emailUsuariRegistrat = '$correoReservadesBicis'";
						$result = mysqli_query($conn, $sqls);

						if (mysqli_num_rows($result)) {
							while($row = mysqli_fetch_assoc($result)) {
								echo $row["ruta"];
								echo "<br><br>";
							}
						}
					?>
				</td>
	    </tr>
	  </tbody>
	</table>

	<br><br>

	<div class="row">
	  <div class="col-sm-6">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title">Editar reserva</h5> <hr>
					<form action="principal.php" method="post">
						<p>- ID: </p>
				    <input type="number" name="IdNecessaries"> <br><br>
						<p>- Bicicletes necessaries: </p>
				    <input type="number" name="bicicletesNecessaries"> <br><br>
				    <p>- Data: </p>
				    <input type="datetime-local" name="dataNecessaria"><br><br>
						<p>- Ruta: </p>
						<select name="rutaNecessaria">
							<?php
								$conn = new mysqli("localhost", "root", "", "biketourbarcelona");
								$sql = "SELECT titol FROM routes";
								$result = $conn->query($sql);

								if ($result->num_rows > 0) {
									while($row = $result->fetch_assoc()) {
										echo "<option>" . $row["titol"] . "</option>";
									}
								}

								$conn->close();
							?>
						</select><br><br>
				    <button type="submit" name="submitUpdates" value="Submit" class="btn btn-info">Actualitzar</button>
				  </form>
					<?php
						if (isset($_POST['submitUpdates'])) {
							$con = mysqli_connect("localhost", "root", "", "biketourbarcelona");
							$bicicletesNecessaries = $_POST['bicicletesNecessaries'];
							$dataNecessaria = $_POST['dataNecessaria'];
							$rutaNecessaria = $_POST['rutaNecessaria'];
							$IdNecessaries = $_POST['IdNecessaries'];
							$sesionEmails = $_SESSION['email'];
							mysqli_query($con,"UPDATE bicicletes
								SET reservades='$bicicletesNecessaries', diaHoraReserva='$dataNecessaria', ruta='$rutaNecessaria'
								WHERE id='$IdNecessaries' AND emailUsuariRegistrat='$sesionEmails'");
							mysqli_close($con);
						}
					?>
	      </div>
	    </div>
	  </div>
	  <div class="col-sm-6">
	    <div class="card">
	      <div class="card-body">
					<h5 class="card-title">Eliminar reserva</h5> <hr>
					<form action="principal.php" method="post">
						- ID: <input type="number" name="idEliminarReserva"><br><br>
						<button type="submit" name="buttonEliminarReserva" class="btn btn-info">Eliminar</button>
					</form>

					<?php
						if (isset($_POST['buttonEliminarReserva'])) {
							$con = mysqli_connect("localhost", "root", "", "biketourbarcelona");
							$idEliminarReserva = $_POST['idEliminarReserva'];
							$sesionEmail = $_SESSION['email'];
							mysqli_query($con,"DELETE FROM bicicletes WHERE id='$idEliminarReserva' AND emailUsuariRegistrat='$sesionEmail'");
							mysqli_close($con);
						}
					?>

	      </div>
	    </div>
	  </div>
	</div>

	<br><br><hr style="background-color: black"><br><br>

	<div class="row">
	  <div class="col-sm-6">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title"><?php echo $titolReservar; ?></h5>
					<p><?php echo $txt; ?></p> <hr>
					<form method="post" id="form_Contacte" action="principal.php">
						<label id="label"><?php echo $bicisAReservar; ?>:</label>
						<input type="number" name="biciReservar">
						<br><br>
						<label id="label"><?php echo $dataReserva; ?>:</label>
						<input type="datetime-local" name="diaHora">
						<br><br>
						<label id="label"><?php echo $tipus; ?>:</label>
						<select name="electrica" required>
							<option value=""><?php echo $opcioTriar; ?></option>
							<option>Electrica</option>
							<option>Tradicional</option>
						</select>
						<br><br>
						<label id="label"><?php echo $menu2; ?>:</label>
						<select name="opcionRuta" required>
							<option value=""><?php echo $opcioTriar; ?></option>
							<option>Fer la meva propia ruta</option>
							<?php
								$conn = mysqli_connect("localhost", "root", "", "biketourbarcelona");
								$sql = "SELECT titol FROM routes";
								$result = mysqli_query($conn, $sql);

								if (mysqli_num_rows($result) > 0) {
									while($row = mysqli_fetch_assoc($result)) {
											echo "<option>" . $row["titol"] . "</option>";
									}
								}
								mysqli_close($conn);
							?>
						</select>
						<br><br>
						<input class="btn btn-primary" id="submit" name="submit" type="submit" value="<?php echo $formulariEnviar; ?>">
					</form>
					<?php
	 	        if (isset($_POST['submit'])) {
	 						$id = 1;
	 						require('conexion.php');
	 						$today = new DateTime("now");
	 						$fecha2 = new DateTime($_POST['diaHora']);
	 						if($today > $fecha2){
	 							header("Location: principal.php?err=Data incorrecte");
	 						}

	 						$consulta = $conexion->prepare("SELECT disponibles FROM bicicletes WHERE diaHoraReserva<='".$_POST["diaHora"]."'");
	 						$consulta->execute();

	 						while ($row = $consulta->fetch()) {
	 					    $disponibles = $row['disponibles'];
	 						}

	 						if ($_POST['biciReservar'] > $disponibles) {
	 							echo "<script>alert('Hi ha masses bicicletes que vols reservar.')</script>";
	 						} else {
	 							echo "<script language='JavaScript'>location.href = 'bicicletesJaReservades.php' </script>";

	 							$conexion = mysqli_connect("localhost", "root", "", "biketourbarcelona");
	 		          $nom = $_POST['nom'];
	 		          $email = $_POST['email'];
	 		          $biciReservar = $_POST['biciReservar'];
	 							$electrica = $_POST['electrica'];
	 							$diaHora = $_POST['diaHora'];
	 							$opcionRuta = $_POST['opcionRuta'];
	 							$disponible = $disponibles - $_POST['biciReservar'];
	 							$emailRegistrat = $_SESSION['email'];

	 		          mysqli_query($conexion,"INSERT INTO bicicletes(reservades,disponibles,diaHoraReserva,ruta,emailUsuariRegistrat,tipus)
	 								VALUES('$biciReservar', $disponible, '$diaHora', '$opcionRuta', '$emailRegistrat', '$electrica')");
	 							mysqli_close($conexion);
	 						}
	 	        }
	 	     ?>
	      </div>
	    </div>
	  </div>
	  <div class="col-sm-6">
	    <div class="card">
	      <div class="card-body">
	        <h5 class="card-title"><?php echo $blogEmpresa; ?></h5>
	        <p class="card-text"><?php echo $comentariBlog; ?></p> <hr>
	        <form method="post" id="form_Contacte" action="principal.php">
						<textarea id="missatgeBlog" name="missatgeBlog" rows="6" class="form-control" placeholder="<?php echo $escriuAqui; ?>" required></textarea>
						<br><br>
						<input id="submit" class="btn btn-primary" name="submits" type="submit" value="<?php echo $formulariEnviar; ?>">
						<?php
							if (isset($_POST['submits'])) {
								$con = mysqli_connect("localhost", "root", "", "biketourbarcelona");
								$texts = $_POST['missatgeBlog'];
								$nomBlog = $_SESSION['nom'];
								$data = date('Y-m-d');
								mysqli_query($con,"INSERT INTO blogEmpresa(nom,texts,data) VALUES('$nomBlog', '$texts','$data')");
								mysqli_close($con);
								echo $blogMissatgeEnviat;
							}
						?>
					</form>
	      </div>
	    </div>
	  </div>
	</div>

	<br> <hr style="background-color: black"> <br>

	<footer class="page-footer font-small">
	  <div class="footer-copyright text-center py-3"><?php echo $paginaUsuari . " - "; ?>
	    <a href="http://localhost:8080/M12-Projecte-Oriol_Mainou/Projecte-Web/"> Bike Tour Barcelona</a>
	  </div>
	</footer>

	<br><br>
</body>
</html>
