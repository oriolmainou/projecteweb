<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Bike Tour Barcelona</title>
    <style>
      .button {
        background-color: #4CAF50; /* Green */
        border-color: black;
        color: white;
        padding: 10px;
        text-align: center;
        font-size: 15px;
        margin: 4px;
        cursor: pointer;
      }
    </style>
  </head>
  <body style="background-color: #F0E68C;">
    <center>
      <h2>
        <u>Taula contacte de la base de dades</u>
      </h2>
    </center>
    <?php
      $idError = "";
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["eliminarFila"])) {
          $idError = "ID is required";
        } else {
          $usuari2 = "root";
          $contrasenya2 = "";
          $servidor2 = "localhost";
          $basededades2 = "biketourbarcelona";

          $conexion2 = mysqli_connect($servidor2, $usuari2, "") or die ("No se ha conectado");
          $db2 = mysqli_select_db($conexion2, $basededades2) or die ("No se ha conectado");

          $consulta2 = "DELETE FROM contacte WHERE id = " . $_POST['eliminarFila'];
          $resultado2 = mysqli_query($conexion2, $consulta2) or die ("No se ha hecho la consulta");
          // var_dump($resultado2);
          mysqli_close($conexion2);
        }
      }
    ?>

    <?php
      $usuari = "root";
      $contrasenya = "";
      $servidor = "localhost";
      $basededades = "biketourbarcelona";

      $conexion = mysqli_connect($servidor, $usuari, "") or die ("No se ha conectado");
      $db = mysqli_select_db($conexion, $basededades) or die ("No se ha conectado");
      $consulta = "SELECT * FROM contacte";
      $resultado = mysqli_query($conexion, $consulta) or die ("No se ha hecho la consulta");

      echo "<table borde='2' style='width:100%' bgcolor='#E3F0FD'>";
      echo "<tr>";
      echo "<th><u>ID MISSATGE</u></th>";
      echo "<th><u>NOM I COGNOMS</u></th>";
      echo "<th><u>CORREU ELECTRONIC</u></th>";
      echo "<th><u>MISSATGE DE CONTACTE</u></th>";
      echo "</tr>";

      while ($columna = mysqli_fetch_array($resultado)){
      	echo "<tr>";
      	echo "<td>" . $columna['id'] . "</td><td>" . $columna['nomCognoms'] . "</td><td>" . $columna['email'] . "</td><td>" . $columna['missatge'] . "</td><td>
        <button type='button' name='botonEliminar' style='background-color: #4CAF50; cursor: pointer; border-color: black; color: white;'>Elimina</button></td>";
        echo "</tr>";
      }

      echo "</table>";
      mysqli_close($conexion);
    ?>

    <br><br>

    <form method="post">
      <input type="number" name="eliminarFila" placeholder="ID de la fila a eliminar...">
      <button type="submit" value="botonElimina">Eliminar</button><br>
      <p>
        <?php
          echo $idError;
        ?>
      </p>
    </form>
    <br><br>
    <button type="button" class="button" onclick="location.href='../iniciarRegistrar/user_admin/principal.php'">Tornar pagina admin</button>
  </body>
</html>
