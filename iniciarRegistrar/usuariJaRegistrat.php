<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Bike Tour Barcelona</title>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="css/css.css">
</head>
<body style="background-color: #DDE3F7;">
  <br>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="card">
          <article class="card-body">
            <center>
              <h3>S'ha registrat correctament!</h3>
            </center>
          </article>
          <div class="border-top card-body text-center"><a href="iniciar.php" target="_blank">Iniciar sessió</a></div>
          <div class="border-top card-body text-center"><a href="../index.php" target="_blank">Tornar a la pàgina</a></div>
        </div>
        <br>
      </div>
    </div>
  </div>
</body>
</html>
