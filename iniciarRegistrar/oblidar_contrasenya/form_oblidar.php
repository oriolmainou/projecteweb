<?php
	session_start();
	require '../../idioma/requirelanguage.php'; // idioma
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="oblidar_contrasenya.css">
    <title>Bike Tour Barcelona</title>
  </head>
  <body>
    <div class="login"><br>
      <h3 class="login-header"><?php echo $Recuperarcontrasenya; ?></h3>
      <form method="post" class="login-container">
        <p><input type="email" id="email" name="email" placeholder="<?php echo $email; ?>"></p>
        <p><input type="password" id="contrasenya" name="password" placeholder="<?php echo $contrasenya; ?>"></p>
        <p><input type="password" id="contrasenya2" name="password2" placeholder="<?php echo $contrasenya; ?>"></p>
        <p><input type="submit" id="enter" name="enter" value="<?php echo $canviarContrasenya; ?>"></p>
        <center>
          <p><a href="../iniciar.php" target="_blank"><?php echo $tornarIniciSessio; ?></a></p>
        </center>
      </form>

      <?php
        if (isset($_POST["enter"])) {
          $valido = true;
          if (!isset($_POST["password"])) {
            echo "<script>alert('Falta contrasenya.')</script>";
            $valido = false;
          }

          if (strlen($_POST["password"]) < 6) {
            echo "<script>alert('Contrasenya menor a 6.')</script>";
            $valido = false;
          }

          if (!isset($_POST["password2"])) {
            echo "<script>alert('Repetir contrasenya està buit.')</script>";
            $valido = false;
          }

          if ($_POST["password2"] != $_POST["password"]) {
            echo "<script>alert('No són iguals les contrasenyes.')</script>";
            $valido = false;
          }

          if ($valido) {
            $usuari = "root";
            $contrasenya = "";
            $servidor = "localhost";
            $basededades = "biketourbarcelona";
            $conexion = mysqli_connect($servidor, $usuari, "") or die ("No se ha conectado");
            $db = mysqli_select_db($conexion, $basededades) or die ("No se ha conectado");

            $encript = md5($_POST["password"]);

            $consulta = "UPDATE persona SET contrasenya='" . $encript . "'  WHERE email='" . $_POST["email"]."'";
            $return = mysqli_query($conexion,$consulta);

            if ($return) {
              echo "Contrasenya cambiada";
            } else {
              echo "No se ha pogut canviar de contrasenya";
            }
          } else {
            echo "<script>alert('Les contrasenyes són diferents.')</script>";
          }
        }
      ?>
    </div>
  </body>
</html>
