<?php
	session_start();

	if (!isset($_SESSION['usuario'])) {
		header('Location: ../loginAdmin.php');
	}

	if (isset($_POST['button'])) {
		session_destroy();
		header('Location: ../../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Bike Tour Barcelona</title>
  <meta charset="utf-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body id="top" style="margin-left: 25px; margin-top: 10px; margin-right: 25px">
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
	    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
				<li class="nav-item active">
	        <a class="nav-link" href="principal.php">Inici</a>
	      </li>
				<li class="nav-item active">
	        <a class="nav-link" href="consultes.php">Consultes</a>
	      </li>
				<li class="nav-item active">
					<a class="nav-link" href="updateBicis.php">Update bicicleta</a>
				</li>
				<li class="nav-item active">
					<b><u><a class="nav-link">RESERVES</a></b></u>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="administradors.php">Administradors</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="blog.php">Blog</a>
				</li>
	    </ul>
			<span class="navbar-text">
				<form method="POST">
					<button type="submit" name="button" id="buttonMarxar" class="btn btn-danger">Log out</button>
				</form>
			</span>
	  </div>
	</nav>

	<br><br>

	<center>
		<h3>Reserves</h3>
	</center>

  <br>

  <?php
    $idError = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      if (empty($_POST["eliminarFila"])) {
        $idError = "ID is required";
      } else {
        $usuari4 = "root";
        $contrasenya4 = "";
        $servidor4 = "localhost";
        $basededades4 = "biketourbarcelona";

        $conexion4 = mysqli_connect($servidor4, $usuari4, "") or die ("No se ha conectado");
        $db4 = mysqli_select_db($conexion4 , $basededades4) or die ("No se ha conectado");

        $consulta4 = "DELETE FROM bicicletes WHERE id = " . $_POST['eliminarFila'];
        $resultado4 = mysqli_query($conexion4, $consulta4) or die ("No se ha hecho la consulta");
        mysqli_close($conexion4);
      }
    }

    $usuari = "root";
    $contrasenya = "";
    $servidor = "localhost";
    $basededades = "biketourbarcelona";

    $conexion = mysqli_connect($servidor, $usuari, "") or die ("No se ha conectado");
    $db = mysqli_select_db($conexion, $basededades) or die ("No se ha conectado");
    $consulta = "SELECT * FROM bicicletes";
    $resultado = mysqli_query($conexion, $consulta) or die ("No se ha hecho la consulta");

    echo "<table borde='2' style='width:100%' bgcolor='#E3F0FD'>";
    echo "<tr>";
    echo "<th><u>ID</u></th>";
    echo "<th><u>EMAIL</u></th>";
    echo "<th><u>RESERVADES</u></th>";
    echo "<th><u>DIA/HORA</u></th>";
    echo "<th><u>RUTA</u></th>";
		echo "<th><u>TIPUS</u></th>";

    echo "</tr>";

    while ($columna = mysqli_fetch_array($resultado)){
      echo "<tr>";
      echo "<td>" . $columna['id'] . "</td><td>" . $columna['emailUsuariRegistrat'] . "</td><td>" . $columna['reservades'] . "</td><td>" . $columna['diaHoraReserva'] . "</td><td>" . $columna['ruta'] . "</td><td>" . $columna['tipus'] . "</td>";
      echo "</tr>";
    }

    echo "</table>";
  ?>
  <br>

  <br><br>
  <form method="post">
    <input type="number" name="eliminarFila" placeholder="ID de la fila a eliminar...">
    <button type="submit" name="botonElimina" value="botonElimina" class="btn btn-info">Eliminar</button><br>
  </form>

	<?php
		if (isset($_POST["botonElimina"])) {
			$conn = mysqli_connect("localhost", "root", "", "biketourbarcelona");
			$id_eliminar = $_POST['eliminarFila'];
			$sql = "DELETE FROM bicicletes WHERE id='$id_eliminar'";

			if (mysqli_query($conn, $sql)) {
			    echo "Eliminat correctament";
			} else {
			    echo "Error";
			}

			mysqli_close($conn);
		}
	?>

  <br><hr><br>

  <center>
		<h3>Actualitzar reserves</h3>
	</center>

  <form method="post" action="reserves.php" style="margin-right: 70%">
		<div class="form-group">
			<label>ID a modificar:</label>
			<input type="number" name="id" required><br>
		</div>
		<div class="form-group">
			<label>Bicicletes reservades:</label>
			<input type="number" name="bicisReservades" required><br>
		</div>
    <div class="form-group">
			<label>Dia i hora:</label>
			<input type="datetime-local" name="diaHora" required><br>
		</div>
		<div class="form-group">
	    <label for="exampleFormControlSelect1">Tipus:</label>
			<select name="electrica" required>
				<option>Electrica</option>
				<option>Tradicional</option>
			</select>
  	</div>

    <button type="submit" name="actualizar" class="btn btn-primary">Actualitzar</button>
	</form>

  <?php

    if (isset($_POST["actualizar"])) {
			$conn = mysqli_connect("localhost", "root", "", "biketourbarcelona");

			$id = $_POST['id'];
			$reservades = $_POST['bicisReservades'];
			$diaHoraReserva = $_POST['diaHora'];
			$tipus = $_POST['electrica'];

			$sql = "UPDATE bicicletes SET reservades='$reservades', diaHoraReserva='$diaHoraReserva', tipus='$tipus' WHERE id='$id'";

			if (mysqli_query($conn, $sql)) {
				echo "Actualitzat!";
			}

			mysqli_close($conn);
    }
  ?>


	<br><hr><br>

	<footer class="page-footer font-small">
	  <div class="footer-copyright text-center py-3">Pàgina administrador:
	    <a href="http://localhost:8080/M12-Projecte-Oriol_Mainou/Projecte-Web/"> Bike Tour Barcelona</a>
	  </div>
	</footer>

</body>
</html>
