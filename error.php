<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Bike Tour Barcelona</title>
    <style>
      .button {
        background-color: #4CAF50;
        border: none;
        color: white;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        margin: 4px 2px;
        cursor: pointer;
      }
    </style>
  </head>
  <body style="background-color: #D8D8D8">
    <?php
      $codes = array(
        400 => array('400 Solicitud incorrecta', 'La solicitud no se ha podido procesar.'),
        401 => array('401 No autorizado', 'El documento o archivo solicitado no se encuentra en este servidor.'),
        403 => array('403 Prohibido', 'La solicitud se ha rechazado.'),
        404 => array('404 No encontrado', 'El servidor no encuentra la página solicitada.'),
        405 => array('405 Método no permitido', 'No se permite el método especificado en la solicitud.'),
        500 => array('500 Error interno', 'The server has refused to fulfill your request.'),
        501 => array('501 No implementado', 'The server has refused to fulfill your request.'),
        502 => array('502 Pasarela incorrecta', 'The server has refused to fulfill your request.'),
        504 => array('504 Tiempo de espera de la pasarela agotado', 'The server has refused to fulfill your request.'),
      );

    ?>

      <center>
        <h1>PAGE NOT FOUND!</h1>
        <p style="font-size: 30px">Press the button to return to Bike Tour Barcelona.</p><br>
        <button class="button" onclick="location.href='http://localhost:8080/M12-Projecte-Oriol_Mainou/Projecte-Web/'">HOME PAGE</button>
      </center>

  </body>
</html>
