<?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "biketourbarcelona";
  $conn = new mysqli($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }
  $conn->set_charset("utf8");
  session_start();
  require 'idioma/requirelanguage.php'; // idioma
?>
<!DOCTYPE html>
<html>
<head>
  <title>Bike Tour Barcelona</title>
  <meta charset="utf-8">
  <script LANGUAGE="JavaScript">
    function abreSitio(){
      var web = document.form1.sitio.options[document.form1.sitio.selectedIndex].value;
      window.open(web);
    }
  </script>
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <meta name="keywords" content="php, multilingüe, multiidioma,website">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>
</head>
<body id="top">
  <div class="wrapper row0">
    <div id="topbar" class="hoc clear"><br>
      <div id="idioma">
        <form name="form1" method="post">
          <select id="idiomas" name="sitio" onChange="javascript:abreSitio()">
            <option><?php echo $idioma ?></option>
            <option value="idioma/changelanguage.php?language=ca">- <?php echo $catala ?></a></option>
            <option value="idioma/changelanguage.php?language=es">- <?php echo $castella ?></a></option>
            <option value="idioma/changelanguage.php?language=en">- <?php echo $angles ?></a></option>
          </select>
        </form>
      </div>
      <div>
        <ul class="nospace inline pushright">
          <li><i class="fa fa-sign-in">&nbsp</i><a href="iniciarRegistrar/iniciar.php" target="_blank"><?php echo $iniciarSessió; ?></a></li>
          <li><i class="fa fa-user">&nbsp</i><a href="iniciarRegistrar/registrar.php" target="_blank"><?php echo $registrar; ?></a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wrapper row1">
    <header id="header" class="hoc clear">
      <nav id="mainav" class="fl_right">
        <ul class="clear">
          <li><a href="index.php"><?php echo $menu1 ?></a></li>
          <li class="active"><a href="rutes.php"><?php echo $menu2 ?></a></li>
          <li><a href="bicicletes.php"><?php echo $menu3 ?></a></li>
          <li><a href="normes.php"><?php echo $menu4 ?></a></li>
          <li><a href="blog.php"><?php echo $menu5 ?></a></li>
          <li><a href="faqs.php"><?php echo $menu6 ?></a></li>
          <li><a href="contacte.php"><?php echo $menu7 ?></a></li>
        </ul>
      </nav>
    </header>
  </div>
  <div class="wrapper row3">
    <main class="hoc container clear">
      <div class="center btmspace-50">
        <h1><b><?php echo $lesNostresRutes; ?></b></h1>
      </div>
      <ul class="nospace group btmspace-50">
        <li class="one_third first">
          <article class="element">
            <figure>
              <a target="_blank" style="color: black" href="https://www.google.es/maps/dir/41.3630926,2.1571632/41.3619214,2.1527031/@41.3617986,2.1524198,15.25z/data=!4m14!4m13!1m10!3m4!1m2!1d2.1683571!2d41.3673291!3s0x12a4a243bfcfc68f:0x3604dfc5ac953c31!3m4!1m2!1d2.1480002!2d41.3657951!3s0x12a4a274264febc5:0x8bf847fded63ebe9!1m0!3e1">
                <?php
                  $connes = mysqli_connect("localhost", "root", "", "biketourbarcelona");

                  $sqles = "SELECT nomImatge, rutaImatge FROM routes WHERE id=9";
                  $resultes = mysqli_query($connes, $sqles);

                  if (mysqli_num_rows($resultes) > 0) {
                    while($rows = mysqli_fetch_assoc($resultes)) {
                        echo '<img src="iniciarRegistrar/user_admin/'. $rows["rutaImatge"] . '"';
                    }
                  } else {
                    echo "0 results";
                  }

                  mysqli_close($connes);

                ?>
              </a>
            </figure>
            <h2>
              <?php
                $sql = "SELECT titol FROM routes WHERE id=9";
                $result = $conn->query($sql);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    echo $row["titol"];
                  }
                }
              ?>
            </h2>
            <p>
              <?php
                $sql = "$sqlSentencia9";
                $result = $conn->query($sql);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    echo $row["$descripcioSentencia9"];
                  }
                }
              ?>
            </p>
            <p><b><?php
              $sql = "SELECT preus FROM routes WHERE id=9";
              $result = $conn->query($sql);
              if (mysqli_num_rows($result) > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                  echo "$preu: " . $row["preus"] . "€/$persona.";
                }
              }
            ?></b></p>
          </article>
        </li>
        <li class="one_third">
          <article class="element">
            <figure>
              <a target="_blank" style="color: black" href="https://www.google.es/maps/dir/41.3630926,2.1571632/41.3619214,2.1527031/@41.3617986,2.1524198,15.25z/data=!4m14!4m13!1m10!3m4!1m2!1d2.1683571!2d41.3673291!3s0x12a4a243bfcfc68f:0x3604dfc5ac953c31!3m4!1m2!1d2.1480002!2d41.3657951!3s0x12a4a274264febc5:0x8bf847fded63ebe9!1m0!3e1">
                <?php
                  $connes = mysqli_connect("localhost", "root", "", "biketourbarcelona");

                  $sqles = "SELECT nomImatge, rutaImatge FROM routes WHERE id=10";
                  $resultes = mysqli_query($connes, $sqles);

                  if (mysqli_num_rows($resultes) > 0) {
                    while($rows = mysqli_fetch_assoc($resultes)) {
                        echo '<img src="iniciarRegistrar/user_admin/'. $rows["rutaImatge"] . '"';
                    }
                  } else {
                    echo "0 results";
                  }

                  mysqli_close($connes);

                ?>
              </a>
            </figure>
            <h2>
              <?php
                $sql = "SELECT titol FROM routes WHERE id=10";
                $result = $conn->query($sql);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    echo $row["titol"];
                  }
                }
              ?>
            </h2>
            <p>
              <?php
                $sql = "$sqlSentencia10";
                $result = $conn->query($sql);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    echo $row["$descripcioSentencia10"];
                  }
                }
              ?>
            </p>
            <p><b><?php
              $sql = "SELECT preus FROM routes WHERE id=10";
              $result = $conn->query($sql);
              if (mysqli_num_rows($result) > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                  echo "$preu: " . $row["preus"] . "€/$persona.";
                }
              }
            ?></b></p>
          </article>
        </li>
        <li class="one_third">
          <article class="element">
            <figure>
              <a target="_blank" style="color: black" href="https://www.google.es/maps/dir/41.3630926,2.1571632/41.3619214,2.1527031/@41.3617986,2.1524198,15.25z/data=!4m14!4m13!1m10!3m4!1m2!1d2.1683571!2d41.3673291!3s0x12a4a243bfcfc68f:0x3604dfc5ac953c31!3m4!1m2!1d2.1480002!2d41.3657951!3s0x12a4a274264febc5:0x8bf847fded63ebe9!1m0!3e1">
                <?php
                  $connes = mysqli_connect("localhost", "root", "", "biketourbarcelona");

                  $sqles = "SELECT nomImatge, rutaImatge FROM routes WHERE id=11";
                  $resultes = mysqli_query($connes, $sqles);

                  if (mysqli_num_rows($resultes) > 0) {
                    while($rows = mysqli_fetch_assoc($resultes)) {
                        echo '<img src="iniciarRegistrar/user_admin/'. $rows["rutaImatge"] . '"';
                    }
                  } else {
                    echo "0 results";
                  }

                  mysqli_close($connes);

                ?>
              </a>
            </figure>
            <h2>
              <?php
                $sql = "SELECT titol FROM routes WHERE id=11";
                $result = $conn->query($sql);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    echo $row["titol"];
                  }
                }
              ?>
            </h2>
            <p>
              <?php
                $sql = "$sqlSentencia11";
                $result = $conn->query($sql);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    echo $row["$descripcioSentencia11"];
                  }
                }
              ?>
            </p>
            <p><b><?php
              $sql = "SELECT preus FROM routes WHERE id=11";
              $result = $conn->query($sql);
              if (mysqli_num_rows($result) > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                  echo "$preu: " . $row["preus"] . "€/$persona.";
                }
              }
            ?></b></p>
          </article>
        </li>
      </ul>
      <ul class="nospace group btmspace-50">
        <li class="one_third first">
          <article class="element">
            <figure>
              <a target="_blank" style="color: black" href="https://www.google.es/maps/dir/41.3630926,2.1571632/41.3619214,2.1527031/@41.3617986,2.1524198,15.25z/data=!4m14!4m13!1m10!3m4!1m2!1d2.1683571!2d41.3673291!3s0x12a4a243bfcfc68f:0x3604dfc5ac953c31!3m4!1m2!1d2.1480002!2d41.3657951!3s0x12a4a274264febc5:0x8bf847fded63ebe9!1m0!3e1">
                <?php
                  $connes = mysqli_connect("localhost", "root", "", "biketourbarcelona");

                  $sqles = "SELECT nomImatge, rutaImatge FROM routes WHERE id=12";
                  $resultes = mysqli_query($connes, $sqles);

                  if (mysqli_num_rows($resultes) > 0) {
                    while($rows = mysqli_fetch_assoc($resultes)) {
                        echo '<img src="iniciarRegistrar/user_admin/'. $rows["rutaImatge"] . '"';
                    }
                  } else {
                    echo "0 results";
                  }

                  mysqli_close($connes);

                ?>
              </a>
            </figure>
            <h2>
              <?php
                $sql = "SELECT titol FROM routes WHERE id=12";
                $result = $conn->query($sql);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    echo $row["titol"];
                  }
                }
              ?>
            </h2>
            <p>
              <?php
                $sql = "$sqlSentencia12";
                $result = $conn->query($sql);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    echo $row["$descripcioSentencia12"];
                  }
                }
              ?>
            </p>
            <p><b><?php
              $sql = "SELECT preus FROM routes WHERE id=12";
              $result = $conn->query($sql);
              if (mysqli_num_rows($result) > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                  echo "$preu: " . $row["preus"] . "€/$persona.";
                }
              }
            ?></b></p>
          </article>
        </li>
        <li class="one_third">
          <article class="element">
            <figure>
              <a target="_blank" style="color: black" href="https://www.google.es/maps/dir/41.3630926,2.1571632/41.3619214,2.1527031/@41.3617986,2.1524198,15.25z/data=!4m14!4m13!1m10!3m4!1m2!1d2.1683571!2d41.3673291!3s0x12a4a243bfcfc68f:0x3604dfc5ac953c31!3m4!1m2!1d2.1480002!2d41.3657951!3s0x12a4a274264febc5:0x8bf847fded63ebe9!1m0!3e1">
                <?php
                  $connes = mysqli_connect("localhost", "root", "", "biketourbarcelona");

                  $sqles = "SELECT nomImatge, rutaImatge FROM routes WHERE id=13";
                  $resultes = mysqli_query($connes, $sqles);

                  if (mysqli_num_rows($resultes) > 0) {
                    while($rows = mysqli_fetch_assoc($resultes)) {
                        echo '<img src="iniciarRegistrar/user_admin/'. $rows["rutaImatge"] . '"';
                    }
                  } else {
                    echo "0 results";
                  }

                  mysqli_close($connes);

                ?>
              </a>
            </figure>
            <h2>
              <?php
                $sql = "SELECT titol FROM routes WHERE id=13";
                $result = $conn->query($sql);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    echo $row["titol"];
                  }
                }
              ?>
            </h2>
            <p>
              <?php
                $sql = "$sqlSentencia13";
                $result = $conn->query($sql);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    echo $row["$descripcioSentencia13"];
                  }
                }
              ?>
            </p>
            <p><b><?php
              $sql = "SELECT preus FROM routes WHERE id=13";
              $result = $conn->query($sql);
              if (mysqli_num_rows($result) > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                  echo "$preu: " . $row["preus"] . "€/$persona.";
                }
              }
            ?></b></p>
          </article>
        </li>
        <li class="one_third">
          <article class="element">
            <figure>
              <a target="_blank" href="https://www.google.es/maps/dir/41.3922199,2.1813542/41.367919,2.18909/@41.3843271,2.1783695,14z/data=!3m1!5s0x12a498f5c19b248d:0x777ed64ea231a704!4m9!4m8!1m5!3m4!1m2!1d2.1972978!2d41.3913242!3s0x12a4a311ce66720d:0xfbb59a2a64ccdc8!1m0!3e1">
                <img src="images/ciutadellaPortVell.png" title="<?php echo $veureRuta; ?>">
              </a>
            </figure>
            <h2>
              <?php
                $sql = "SELECT titol FROM routes WHERE id=14";
                $result = $conn->query($sql);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    echo $row["titol"];
                  }
                }
              ?>
            </h2>
            <p>
              <?php
                $sql = "$sqlSentencia14";
                $result = $conn->query($sql);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    echo $row["$descripcioSentencia14"];
                  }
                }
              ?>
            </p>
            <p><b><?php
              $sql = "SELECT preus FROM routes WHERE id=14";
              $result = $conn->query($sql);
              if (mysqli_num_rows($result) > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                  echo "$preu: " . $row["preus"] . "€/$persona.";
                }
              }
            ?></b></p>
          </article>
        </li>
      </ul>
      <nav class="pagination">
          <ul>
            <li><a href="rutes.php">&laquo; <?php echo $anterior; ?></a></li>
            <li><a href="rutes.php">1</a></li>
            <li id="liPagina"><a>2</a></li>
            <li><a href="rutes3.php">3</a></li>
            <li><a href="rutes4.php">4</a></li>
            <li><a href="rutes3.php"><?php echo $seguent; ?> &raquo;</a></li>
          </ul>
        </nav>
    </main>
  </div>
  <div class="wrapper row4 bgded overlay" style="background-color: #FAFAFA;">
    <footer id="footer" class="hoc clear">
      <div class="one_quarter first">
        <h6 class="title">Bike Tour Barcelona</h6>
        <p><?php echo $descripcio; ?></p>
      </div>
      <div class="one_quarter">
        <h6 class="title"><?php echo $contacteAmbNosaltres; ?></h6>
        <ul class="nospace linklist contact">
          <li><i class="fa fa-map-marker"></i>
            <address>Plaça del Nord 14 <br>08029 Barcelona</address>
          </li>
          <li><i class="fa fa-phone"></i>+34 934.547.411</li>
          <li><i class="fa fa-envelope-o"></i>biketour@bcnbiketour.cat</li>
        </ul>
      </div>
      <div class="one_quarter">
        <h6 class="title"><?php echo $xarxesSocials; ?></h6>
        <ul class="nospace linklist contact">
          <li><i class="fa fa-twitter-square"></i><a style="color: white" href="#" title="Twitter">Twitter</a></li>
          <li><i class="fa fa-facebook-square"></i><a style="color: white" href="#" title="Facebook">Facebook</a></li>
          <li><i class="fa fa-instagram"></i><a style="color: white" href="#" title="Instagram">Instagram</a></li>
        </ul>
      </div>
      <div>
        <?php temps(); ?>
      </div>
    </footer>
  </div>
  <!-- -->
  <!-- JAVASCRIPTS -->
  <script src="layout/scripts/jquery.min.js"></script>
  <script src="layout/scripts/jquery.backtotop.js"></script>
  <script src="layout/scripts/jquery.mobilemenu.js"></script>
</body>
</html>
