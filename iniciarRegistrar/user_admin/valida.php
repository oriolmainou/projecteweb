<?php
  session_start();

  if (isset($_SESSION['usuario'])) {
    header('Location: principal.php');
  }

  if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $usuario = $_POST['usuario'];
    $password = $_POST['clave'];
    $desencript = md5($password);
    require('conexion.php');

    $consulta = $conexion->prepare('SELECT * FROM administrador WHERE nom=:usuario AND contrasenya=:password');
    $consulta->execute(array(':usuario'=> $usuario, ':password'=>$desencript));
    $resultado = $consulta->fetch();

    if ($resultado != false) {
      $_SESSION['usuario'] = $usuario;
      header('Location: principal.php');
    } else {
      header("Location: ../loginAdmin.php");
    }
  }
?>
