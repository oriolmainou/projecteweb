<?php
	session_start();
	if (isset($_SESSION['usuario'])) {
		header('Location: user_admin/principal.php');
	}
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Bike Tour Barcelona</title>
		<link rel="stylesheet" type="text/css" href="user_admin/css_loginAdmin.css">
  </head>
  <body>
    <form id="login" method="post" action="user_admin/valida.php" >
      <h1 id="titulo">Usuari administrador</h1>
      <input type="text" id="input" placeholder="Correu electrònic..." name="usuario" required>
      <input type="password" id="input" name="clave" placeholder="Contrasenya..." required>
      <center>
				<button id="loginButton" name="formulariBoton">Entrar</button>
      </center>
			<br><hr><br>
			<div>
				<center>
					<p style="font-size: 14px;">Vols entrar com a usuari? Entra <a href="iniciar.php" target="_blank">aquí</a></p>
				</center>
			</div>
  </form>
  </body>
</html>
