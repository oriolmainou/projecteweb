-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-06-2019 a las 09:54:05
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `biketourbarcelona`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id_admin` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `contrasenya` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id_admin`, `nom`, `contrasenya`) VALUES
(1, 'admin2@biketourbarcelona.cat', 'fd23d0fc04f714b7475cde36ec4ccec4'),
(2, 'admin@biketourbarcelona.cat', 'fd23d0fc04f714b7475cde36ec4ccec4');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bicicletes`
--

CREATE TABLE `bicicletes` (
  `id` int(11) NOT NULL,
  `reservades` int(11) DEFAULT NULL,
  `disponibles` int(11) NOT NULL,
  `tornades` int(11) NOT NULL,
  `diaHoraReserva` datetime DEFAULT NULL,
  `ruta` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `emailUsuariRegistrat` varchar(50) CHARACTER SET latin1 NOT NULL,
  `tipus` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `bicicletes`
--

INSERT INTO `bicicletes` (`id`, `reservades`, `disponibles`, `tornades`, `diaHoraReserva`, `ruta`, `emailUsuariRegistrat`, `tipus`) VALUES
(1, 0, 30, 0, '2020-12-01 20:20:00', 'NULL', 'primervalor@primervalor.cat', 'NULL'),
(3, 5, 25, 0, '2020-12-12 19:15:00', 'Forum / Parc De La Ciutadella', 'adrienmoerman45@gmail.com', 'Electrica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blogempresa`
--

CREATE TABLE `blogempresa` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `texts` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `blogempresa`
--

INSERT INTO `blogempresa` (`id`, `nom`, `texts`, `data`) VALUES
(13, 'Adrien', 'Una empresa que sempre ens ha servit per llogar bicicletes. Un 10.', '2019-05-03'),
(14, 'Oriol', '\0Empresa molt ben situada, en han tractat be amb uns preus bons...', '2019-05-06'),
(15, 'Marta', '\0Vam llogar les bicicletes a Montjuic, vam tenir un problema i ens van ajudar rapidament', '2019-05-09'),
(16, 'Jordi', '\0Bike Tour te bones rutes!!!', '2019-05-10'),
(22, 'Adrien', 'Presentacio', '2019-05-22'),
(26, 'Mireia', 'I''m happy with the company', '2019-06-02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client`
--

CREATE TABLE `client` (
  `id_client` int(11) NOT NULL,
  `numTargeta` varchar(30) NOT NULL,
  `poblacio` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `client`
--

INSERT INTO `client` (`id_client`, `numTargeta`, `poblacio`) VALUES
(1, '8973894c8b21393918af61a6a82f08', 'Barcelona'),
(9, '8973894c8b21393918af61a6a82f08', '48125874a'),
(14, '8973894c8b21393918af61a6a82f08', 'Valencia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacte`
--

CREATE TABLE `contacte` (
  `id` int(11) NOT NULL,
  `nomCognoms` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `missatge` varchar(12500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacte`
--

INSERT INTO `contacte` (`id`, `nomCognoms`, `email`, `missatge`) VALUES
(7, 'Marti Miguel', 'miguel@salle.cat', 'Estem provant si tot funciona correctament.'),
(8, 'Pep Abertos', 'pepe@salle.net', 'Provant el PHP');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `titol` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcio` text COLLATE utf8_unicode_ci NOT NULL,
  `titolCastella` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcioCastella` text COLLATE utf8_unicode_ci NOT NULL,
  `titolAngles` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcioAngles` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `faqs`
--

INSERT INTO `faqs` (`id`, `titol`, `descripcio`, `titolCastella`, `descripcioCastella`, `titolAngles`, `descripcioAngles`) VALUES
(1, 'Com reservo una bicicleta i una ruta?', 'Pots reservar la teva ruta i bicicleta al nostre formulari d''usuari. T''has de registrar i quan entris com usuari apareix el formulari per reservar ruta i bicicletes.', '&#191Como reservo una bicicleta y una ruta?', 'Puedes reservar tu ruta y bicicleta con nuestro formulario de usuario. Te tienes que registrar y cuando entres como usuario aparece el formulario para reservar ruta y bicicletas.', 'How can I book a bike and a route?', 'You have to register. When you enter, as a user, the form to book route and bicycles appears.'),
(2, 'Que puc fer per emportar-me la bicicleta de lloguer?', 'Abans de tot s''ha de passar per caixa per pagar el deposit de 30 euros/persona (es torna quan es tornen les bicicletes) i pagar el preu de la ruta. ', '&#191Que puedo hacer para llevarme la bicicleta de alquiler?', 'Antes de todo hay que pasar por caja para pagar el dep&#243sito de 30 euros / persona (se vuelve cuando se vuelven las bicicletas) y pagar el precio de la ruta.', 'What can I do to take my rental bike?', 'First of all you have to pay the deposit of 30 euros / person (it is returned when the bicycles are returned) and you have to pay the price of the route.'),
(3, 'Com s''efectua el pagament?', 'El pagament es fa abans de l''inici de la ruta.', '&#191C&#243mo se efect&#250a el pago?', 'El pago se hace antes del inicio de la ruta.', 'How is the payment effected?', 'You have to make the payment before the start of the route.'),
(4, 'He de portar casc?', 'Si, a Barcelona s''ha de portar casc imprescindiblement.', '&#191Debo usar un casco?', 'Si, en Barcelona debes llevar casco imprescindiblemente.', 'Should I wear a helmet?', 'Yes, in Barcelona you have to wear a helmet.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `normes`
--

CREATE TABLE `normes` (
  `id` int(11) NOT NULL,
  `descripcio` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcioCastella` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcioAngles` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `normes`
--

INSERT INTO `normes` (`id`, `descripcio`, `descripcioCastella`, `descripcioAngles`) VALUES
(1, 'L''usuari de la bicicleta es compromet a respectar les lleis i normes de tr&#224fic i &#233s el responsable de tots els desperfectes causats com un accident durant la seva ruta, i coneix els riscos que implica conduir una bicicleta i &#233s l''&#250nic responsable dels danys causats. ', 'El usuario de la bicicleta se compromete a respetar las leyes y normas de tr&#225fico y es el responsable de todos los desperfectos causados, y conoce los riesgos que implica conducir una bicicleta.', 'The user of the bicycle is committed to respect the laws and traffic regulations and she/he is responsible for all the damage caused, and he/she knows the risks involved in riding a bicycle.'),
(2, 'La persona que reserva les bicicletes &#233s el responsable del manteniment de la bicicleta en el moment en que l''utilitza. ', 'La persona que utiliza las bicicletas es el responsable del mantenimiento de la bicicleta en el momento en que lo utiliza.', 'The person who uses the bicycles is responsible for the maintenance of the bicycle at the time it is used.'),
(3, 'Totes les bicicletes han de ser tornades per la mateixa persona amb el mateix estat en que les han recollit. ', 'Todas las bicicletas deben ser devueltas por la misma persona con el mismo estado en que las han recogido.', 'All bicycles must be returned by the same person with the same status in which they have collected them.'),
(4, 'En cas de p&#232rdua de la bicicleta, l''usuari haur&#224 d''avisar el m&#233s r&#224pid possible per poder localitzar-la. ', 'Si vosotros perd&#233is la bicicleta, deber&#233is avisar lo m&#225s r&#225pido posible para poder localizarla.', 'If you lose your bike, you will have to notify as quickly as possible to be able to locate it.'),
(5, 'Com a fian&#231a, es demana una q&#252antitat de diners per llogar les bicicletes que ser&#224 tornat a la devoluci&#243 de les bicicletes. ', 'Como fianza, se pide una cantidad de dinero para alquilar las bicicletas que ser&#225 devuelto a la devoluci&#243n de las bicicletas.', 'As a guarantee, we need a bit of money for you to rent your bicycles and we will return it when you return the bicycles.'),
(6, 'El pagament sempre ser&#224 a trav&#233s de la p&#224gina per adelantat o en el mateix moment de recollir les bicicletes. ', 'El pago siempre ser&#225 a trav&#233s de la p&#225gina por adelantado o en el mismo momento de recoger las bicicletas.', 'The payment will always be through the page in advance or at the same time to collect the bicycles.'),
(7, 'En cas d''averia tenim un tel&#232fon de contacte i et portarem una altre bicicleta.', 'En caso de aver&#237a tenemos un tel&#233fono de contacto y nosotros te llevaremos otra bicicleta.', 'In case of failure we have a contact telephone number and we will take you another bike.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id_persona` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `cognoms` varchar(50) NOT NULL,
  `dni` varchar(25) NOT NULL,
  `contrasenya` varchar(100) NOT NULL,
  `email` varchar(55) NOT NULL,
  `nacionalitat` varchar(50) NOT NULL,
  `mobil` int(11) NOT NULL,
  `dataNaixement` date NOT NULL,
  `sexe` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id_persona`, `nom`, `cognoms`, `dni`, `contrasenya`, `email`, `nacionalitat`, `mobil`, `dataNaixement`, `sexe`) VALUES
(1, 'Oriol', 'Mainou', '48102503G', '939654791c067dfb9021a9151bb95922', 'oriolmainou97@gmail.com', 'Spain', 619834308, '1997-11-14', 'home'),
(9, 'Adrien', 'Moerman', '1236547P', '939654791c067dfb9021a9151bb95922', 'adrienmoerman45@gmail.com', 'Spain', 2147483647, '2000-02-22', 'home'),
(12, 'Joaquim', 'Volta', '56987410A', '939654791c067dfb9021a9151bb95922', 'joaquim@gmail.com', 'Spain', 654789654, '2000-01-16', 'home'),
(13, 'Mireia', 'Bonanova', '20147853A', '939654791c067dfb9021a9151bb95922', 'mireia@hotmail.com', 'Spain', 654789600, '1995-05-13', 'dona'),
(14, 'Pedro', 'Pepe', '56321478N', '939654791c067dfb9021a9151bb95922', 'pedropepe@gmail.com', 'Spain', 2147483647, '2000-11-12', 'home');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personabicicletes`
--

CREATE TABLE `personabicicletes` (
  `id_persona` int(11) NOT NULL,
  `id_bicicleta` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `routes`
--

CREATE TABLE `routes` (
  `id` int(11) NOT NULL,
  `titol` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcio` text CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  `descripcioCastella` text COLLATE utf8_unicode_ci NOT NULL,
  `descripcioAngles` text COLLATE utf8_unicode_ci NOT NULL,
  `preus` double NOT NULL,
  `nomImatge` text COLLATE utf8_unicode_ci NOT NULL,
  `rutaImatge` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `routes`
--

INSERT INTO `routes` (`id`, `titol`, `descripcio`, `descripcioCastella`, `descripcioAngles`, `preus`, `nomImatge`, `rutaImatge`) VALUES
(1, 'Montju&#239c', 'La muntanya de Montju&#239c &#233s un bon lloc per veure la ciutat enlairada. Podrem visitar Estadi Ol&#237mpic i el Palau Sant Jordi.', 'La monta&#241a de Montju&#239c es un buen lugar para ver la ciudad elevada. Podremos visitar Estadio Ol&#237mpico y el Palau Sant Jordi.', 'The mountain of Montju&#239c is a good place to see the elevated city. We can visit the Olympic Stadium and the Palau Sant Jordi.', 5.5, 'estadiolimpic.jpg', 'imagenes/estadiolimpic.jpg'),
(2, 'Tur&#237stica', 'Coneixem plaça Catalunya, el Gòtic, Parc de la Ciutadella, Sagrada Família, Passeig de Gràcia, La Pedrera i La Casa Batlló.', 'Conocemos Plaça Cataluña, el Gótico, Parque de la Ciutadella, Sagrada Familia, Paseo de Gracia, La Pedrera y La Casa Batlló.', 'We know Plaça Catalunya, the Gothic, Ciutadella Park, Sagrada Familia, Paseo de Gracia, La Pedrera and La Casa Batlló.', 6, 'Cat.jpg', 'imagenes/Cat.jpg'),
(3, 'Passeig entre parcs', 'En ella es pot comen&#231ar al Parc del Guinardó. Anirem cap al Turó de la Rovira i pujarem cap als bunkers i acabarem al Parc Güell.', 'En ella se puede empezar en el Parque del Guinardó. Iremos hacia el Turó de la Rovira y subiremos hacia los bunkers y se acaba en el Parc Güell.', 'You can start at the Parc del Guinardó. We will go to the Turó de la Rovira and we will go up to the bunkers and we''ll finish at the Güell Park.', 10.95, 'guell.jpg', 'imagenes/guell.jpg'),
(4, 'Carretera De Les Aigues', 'Situat a Collserola, aquesta ruta s''inicia a l''avinguda del Tibidabo i acaba a Esplugues. En ella veurem les millors panor&#224miques de Barcelona.', 'Situado en Collserola, esta ruta se inicia en la avenida del Tibidabo y termina en Esplugues. En ella veremos las mejores panor&#225micas de Barcelona.', 'Located in Collserola, this route starts at the avenue of Tibidabo and ends at Esplugues. Here we will see the best panoramic views of Barcelona.', 13.5, 'aigues.jpg', 'imagenes/aigues.jpg'),
(5, 'Vila Ol&#237mpica', 'En ella, es veu el Moll de la Fusta, la Barceloneta i acabarem a la Vila Ol&#237mpica on veurem on vivien els atletes en els JJOO 1992.', 'En ella, se ve el Moll de la Fusta, la Barceloneta y acabaremos en la Villa Ol&#237mpica donde veremos donde viv&#237an los atletas en los JJOO 1992.', 'We see Moll de la Fusta, Barceloneta and finish in the Olympic Village where we''ll see where the athletes lived in ''92 the Olympic Games.', 12, 'vilaOlimpica.jpg', 'imagenes/vilaOlimpica.jpg'),
(6, 'Passeig Mar&#237tim', 'La ruta comen&#231a a la zona del Hotel Wela i acaba a la zona Municipal de la Mar Bella. Recorrem la platja de la ciutat de Barcelona.', 'La ruta empieza en la zona del Hotel Wela y termina en la zona Municipal de la Mar Bella. Recorremos la playa de la ciudad de Barcelona.', 'The route begins in the area of the Hotel Wela and it ends in the Municipal area of the Mar Bella. We will also see the beach of the city of Barcelona.', 15.8, 'maritim.jpg', 'imagenes/maritim.jpg'),
(7, 'F&#242rum', 'Recorrem la zona del F&#242rum. &#201s bona per fer amb fam&#237lia. En aquesta ruta, i si esteu interessats, es pot afegir el Bosc Urb&#224.', 'Recorremos la zona del F&#242rum. Es buena para hacer en familia. En esta ruta, y si est&#225n interesados, se puede a&#241adir el Bosque Urbano.', 'We travel through the Forum area. It''s good to do with family. On this route, and if you are interested, you can add the Urban Forest.', 14, 'forum.jpg', 'imagenes/forum.jpg'),
(8, 'Diagonal / Camp Nou', 'La ruta recorre la zona de la Diagonal, comen&#231ant als ''Jardinets de Gr&#224cia'', fins arribar a l''Estadi Camp Nou. Inclou entrada al museu.', 'La ruta recorre la zona de la Diagonal empezando a ''Jardinets de Gracia'', hasta llegar al Estadio Camp Nou. Incluye entrada al museo.', 'The route runs through the Diagonal area, starting at Gracia, until you reach the Camp Nou Stadium. Includes tickets to the museum.', 15.5, 'campnou.jpg', 'imagenes/campnou.jpg'),
(9, 'El Centre De Barcelona', 'Recorrem la plaça Catalunya, el Passeig de Gràcia, La Pedrera i La Casa Batlló. Coneixem la zona més turística de la ciutat.', 'Recorremos la plaza Cataluña, el Paseo de Gracia, La Pedrera y La Casa Batlló. Conocemos la zona más turística de la ciudad.', 'In it, we visit Plaça Catalunya, Passeig de Gràcia, La Pedrera and Casa Batlló. We know the most touristy part of the city.', 15.75, 'Cat.jpg', 'imagenes/Cat.jpg'),
(10, 'Parc De La Ciutadella', 'Situat al centre de Barcelona, recorrem tot sencer el parc i visitem els jardins del Parlament català. En el tour tenim accés al Zoo.', 'Situado en el centro de Barcelona, recorremos todo entero el parque y visitamos los jardines del Parlamento catalán. En el tour hay acceso al Zoo.', 'Located in the center of Barcelona, we visit the park intact, and we visit the gardens of the Catalan Parliament. In the tour we have access to the Zoo.', 14.85, 'ciutadella.jpg', 'imagenes/ciutadella.jpg'),
(11, 'Montju&#239c / Sagrada Fam&#237lia', 'Montju&#241c és un lloc per veure la ciutat. La ruta comença al Estadi i acaba a la basílica de la Sagrada Familia', 'Montju&#241c es un lugar para ver la ciudad. La ruta comienza en el Estadio y termina en la Sagrada Familia.', 'Montju&#241c is a place to see the city. The route begins at the Stadium and it ends at La Sagrada Familia', 13.95, 'teleferic.jpg', 'imagenes/teleferic.jpg'),
(12, 'La Pedrera / Zona Universit&#224ria', 'La Pedrera és un edifici que es troba al Passeig de Gràcia. Aquí comença la ruta i acaba a la Zona Universitària. Bona ruta en familia.', 'La Pedrera es un edificio que se encuentra en el Paseo de Gracia. Aquí comienza la ruta y termina en la Zona Universitaria.', 'La Pedrera is a building that is located on Passeig de Gracia. Here it begins the route and it ends at the University Zone.', 15.3, 'pedrera.jpg', 'imagenes/pedrera.jpg'),
(13, 'Parc Natural de Collserola', 'La ruta consisteix en conéixer tot el Parc Natural de Collserola. En la ruta es té accés al parc atraccions del Tibidabo.', 'La ruta consiste en conocer todo el Parque Natural de Collserola. En la ruta se tiene acceso al parque atracciones del Tibidabo.', 'The route consists of knowing the Collserola Natural Park. On the route you have access to the Tibidabo thematic park.', 13.1, 'collserola.jpg', 'imagenes/collserola.jpg'),
(14, 'Parc De La Ciutadella / Port Vell', 'Recorrem tot sencer el parc i visitem els jardins del Parlament. El tour acaba a la platja, al Port Vell davant Hotel Wela.', 'Recorremos todo entero el parque y visitamos los jardines del Parlamento. El tour termina en la playa, en el Port Vell ante Hotel Wela.', 'We visited the park and visited the Parliament Gardens. The tour ends at the beach, in Port Vell before Hotel Wela.', 17.45, 'pvell.jpg', 'imagenes/pvell.jpg'),
(15, 'Pla&#231a Kennedy / Lesseps', 'La ruta comença a la Plaça Kennedy, inici de Avinguda Tibidado, i acaba a la Plaça Lesseps. En ella coneixerem el barri de Sant Gervasi.', 'La ruta empieza en la Plaza Kennedy, inicio de Avenida Tibidabo, y termina en la Plaza Lesseps. En ella conoceremos el barrio de Sant Gervasi.', 'The route begins at Kennedy Square, beginning of Tibidado Avenue, and it ends at Lesseps. In it we will know the district of Sant Gervasi.', 14.75, 'lesseps.jpg', 'imagenes/lesseps.jpg'),
(16, 'Park G&#252ell / Lesseps', 'La ruta comença al Park Güell. En ella coneixerem els llocs més importants del Park i del Barri de Gràcia. Acaba a la Plaça Lesseps.', 'La ruta empieza en el Park Güell. En ella conoceremos los lugares más importantes del Park y del Barrio de Gracia. Termina en la Plaza Lesseps.', 'The route begins at Park Güell. In it, we will know the most important places of the Park and the Barrio de Gracia. It ends at Lesseps Square.', 15.99, 'guell.jpg', 'imagenes/guell.jpg'),
(17, 'Monestir De Pedralbes / Camp Nou', 'La ruta comença al Monestir de Pedralbes. En ella es te accés al monestir i es baixa cap al Camp Nou. La ruta conté entrades pel museu.', 'La ruta comienza en el Monasterio de Pedralbes. En ella, se tiene acceso al monasterio y se baja hacia el Camp Nou. La ruta contiene entradas para el museo.', 'The route begins at the Monastery of Pedralbes. In it, you have access to the monastery and you go down to the Camp Nou. The route contains tickets to the museum.', 8.7, 'camp-nou.jpg', 'imagenes/camp-nou.jpg'),
(18, 'Pla&#231a Catalunya / Pla&#231a Espanya', 'La ruta comença a la Plaça Catalunya de Barcelona. En ella recorrem les Rambles fins Drassanes i del Paral·lel a Plaça Espanya.', 'La ruta comienza en la Plaza Catalunya de Barcelona. En ella recorremos las Ramblas hasta Drassanes y del Paralelo a Plaza España. La ruta contiene entradas para las Arenas.', 'The route begins at Plaça Catalunya in Barcelona. In it, we visit the Ramblas to Drassanes and the Parallel to Plaça Espanya. The route contains tickets for the Arenas.', 17.45, 'espanya.jpg', 'imagenes/espanya.jpg'),
(19, 'F&#242rum / Parc De La Ciutadella', 'La ruta comença al parc del Forum. En ella recorrem tot el Forum i les Glòries i arribarem al Parc de la Ciutadella. La ruta conté entrades pel zoo.', 'La ruta comienza en el parque del Forum. En ella recorremos todo el Forum y Glòries y llegaremos al Parque de la Ciutadella. La ruta contiene entradas para el zoo.', 'The route begins at the Forum park. In it we visit through the Forum and the Glories and we will arrive at the Parc de la Ciutadella. The route contains tickets for the zoo.', 15, 'forum.jpg', 'imagenes/forum.jpg'),
(20, 'Catedral / Sagrada Fam&#237lia', 'En la ruta recorrem la zona de La Catedral i la basilica de la Sagrada Familia. La ruta conté entrades per la basílica.', 'En la ruta recorremos la zona de La Catedral y la Sagrada Familia. La ruta contiene entradas para la basílica de la Sagrada Familia.', 'On this route, we visit the area of La Catedral and La Sagrada Familia. The route contains tickets to the basilica of La Sagrada Familia.', 15.99, 'catedral.jpg', 'imagenes/catedral.jpg'),
(21, 'Parc Del Guinard&#243 / La Pedrera', 'En la ruta recorrem el Parc del Guinardó i la Casa Milà (La Pedrera). Coneixem el barri del Guinardó seguit del barri de Gràcia.', 'En la ruta recorremos el Parque del Guinardó y la Casa Milà (La Pedrera). Conocemos el barrio del Guinardó seguido del barrio de Gracia.', 'On the route we cross the Parc del Guinardó and Casa Milà (La Pedrera). We know the neighborhood of Guinardó and the district of Gràcia.', 14.95, 'pedrera.jpg', 'imagenes/pedrera.jpg'),
(22, 'Ruta Lliure', 'En ruta lliure el client pot escollir la ruta que desitja fer dintre àrea metropolitana de Barcelona. L''usuari ha indicar el punt inici i el punt final. Bona ruta per fer en familia.', 'En ruta libre el cliente puede elegir la ruta que desea hacer dentro área metropolitana de Barcelona. El usuario debe indicar el punto inicio y el punto final.', 'On a free route, the client can choose the route that he wishes to do in the metropolitan area of Barcelona. The user must indicate the starting point and the end point.', 25.95, 'pciutadella.jpg', 'imagenes/pciutadella.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `treballador`
--

CREATE TABLE `treballador` (
  `id_treballador` int(11) NOT NULL,
  `compteBancari` varchar(100) NOT NULL,
  `seguretatSocial` varchar(100) NOT NULL,
  `salari` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `treballador`
--

INSERT INTO `treballador` (`id_treballador`, `compteBancari`, `seguretatSocial`, `salari`) VALUES
(13, '40eab3d645e5d42a88747ee454486bb6', 'e807f1fcf82d132f9bb018ca6738a1', 1214),
(14, '40eab3d645e5d42a88747ee454486bb6', 'e807f1fcf82d132f9bb018ca6738a1', 1232);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`nom`);

--
-- Indices de la tabla `bicicletes`
--
ALTER TABLE `bicicletes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `blogempresa`
--
ALTER TABLE `blogempresa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id_client`);

--
-- Indices de la tabla `contacte`
--
ALTER TABLE `contacte`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `normes`
--
ALTER TABLE `normes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id_persona`);

--
-- Indices de la tabla `personabicicletes`
--
ALTER TABLE `personabicicletes`
  ADD PRIMARY KEY (`id_bicicleta`,`id_persona`),
  ADD KEY `fk1` (`id_persona`);

--
-- Indices de la tabla `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `treballador`
--
ALTER TABLE `treballador`
  ADD PRIMARY KEY (`id_treballador`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bicicletes`
--
ALTER TABLE `bicicletes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `blogempresa`
--
ALTER TABLE `blogempresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `contacte`
--
ALTER TABLE `contacte`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `normes`
--
ALTER TABLE `normes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id_persona` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `routes`
--
ALTER TABLE `routes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `treballador`
--
ALTER TABLE `treballador`
  MODIFY `id_treballador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `fkclient` FOREIGN KEY (`id_client`) REFERENCES `persona` (`id_persona`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `personabicicletes`
--
ALTER TABLE `personabicicletes`
  ADD CONSTRAINT `fk1` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk2` FOREIGN KEY (`id_bicicleta`) REFERENCES `bicicletes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `treballador`
--
ALTER TABLE `treballador`
  ADD CONSTRAINT `fk_treballador` FOREIGN KEY (`id_treballador`) REFERENCES `persona` (`id_persona`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
