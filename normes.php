<?php
  session_start();
  require 'idioma/requirelanguage.php'; // idioma
?>

<!DOCTYPE html>
<html>
<head>
  <title>Bike Tour Barcelona</title>
  <meta charset="utf-8">
  <script LANGUAGE="JavaScript">
    function abreSitio(){
      var web = document.form1.sitio.options[document.form1.sitio.selectedIndex].value;
      window.open(web);
    }
  </script>
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <meta name="keywords" content="php, multilingüe, multiidioma,website">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>
</head>
<body id="top">
  <div class="wrapper row0">
    <div id="topbar" class="hoc clear"><br>
      <div id="idioma">
        <form name="form1" method="post">
          <select id="idiomas" name="sitio" onChange="javascript:abreSitio()">
            <option><?php echo $idioma ?></option>
            <option value="idioma/changelanguage.php?language=ca">- <?php echo $catala ?></a></option>
            <option value="idioma/changelanguage.php?language=es">- <?php echo $castella ?></a></option>
            <option value="idioma/changelanguage.php?language=en">- <?php echo $angles ?></a></option>
          </select>
        </form>
      </div>
      <div>
        <ul class="nospace inline pushright">
          <li><i class="fa fa-sign-in">&nbsp</i><a href="iniciarRegistrar/iniciar.php" target="_blank"><?php echo $iniciarSessió; ?></a></li>
          <li><i class="fa fa-user">&nbsp</i><a href="iniciarRegistrar/registrar.php" target="_blank"><?php echo $registrar; ?></a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wrapper row1">
    <header id="header" class="hoc clear">
      <nav id="mainav" class="fl_right">
        <ul class="clear">
          <li><a href="index.php"><?php echo $menu1 ?></a></li>
          <li><a href="rutes.php"><?php echo $menu2 ?></a></li>
          <li><a href="bicicletes.php"><?php echo $menu3 ?></a></li>
          <li class="active"><a href="normes.php"><?php echo $menu4 ?></a></li>
          <li><a href="blog.php"><?php echo $menu5 ?></a></li>
          <li><a href="faqs.php"><?php echo $menu6 ?></a></li>
          <li><a href="contacte.php"><?php echo $menu7 ?></a></li>
        </ul>
      </nav>
    </header>
  </div>
  <div class="wrapper row3">
    <main class="hoc container clear"><center>
      <h1 id="normesBici"><u><?php echo $normes; ?></u></h1><br>
      <p id="textNormes" style="text-align: left;">
        <?php
          $servername = "localhost";
          $username = "root";
          $password = "";
          $dbname = "biketourbarcelona";

          $conn = mysqli_connect($servername, $username, $password, $dbname);
          $sql = "SELECT * FROM normes";
          $result = mysqli_query($conn, $sql);

          if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
              echo "- " . $row[$descrip] . "<br><br>";
            }
          }

          mysqli_close($conn);
        ?>
      </p>
      <br><br>
      <img id="fotoCatedral" src="images/catedral.jpg" title="La Catedral Barcelona">
    </main>
  </div>
  <div class="wrapper row4 bgded overlay" style="background-color: #FAFAFA;">
    <footer id="footer" class="hoc clear">
      <div class="one_quarter first">
        <h6 class="title">Bike Tour Barcelona</h6>
        <p><?php echo $descripcio; ?></p>
      </div>
      <div class="one_quarter">
        <h6 class="title"><?php echo $contacteAmbNosaltres; ?></h6>
        <ul class="nospace linklist contact">
          <li><i class="fa fa-map-marker"></i>
            <address>Plaça del Nord 14 <br>08029 Barcelona</address>
          </li>
          <li><i class="fa fa-phone"></i>+34 934.547.411</li>
          <li><i class="fa fa-envelope-o"></i>biketour@bcnbiketour.cat</li>
        </ul>
      </div>
      <div class="one_quarter">
        <h6 class="title"><?php echo $xarxesSocials; ?></h6>
        <ul class="nospace linklist contact">
          <li><i class="fa fa-twitter-square"></i><a style="color: white" href="#" title="Twitter">Twitter</a></li>
          <li><i class="fa fa-facebook-square"></i><a style="color: white" href="#" title="Facebook">Facebook</a></li>
          <li><i class="fa fa-instagram"></i><a style="color: white" href="#" title="Instagram">Instagram</a></li>
        </ul>
      </div>
      <div>
        <?php temps(); ?>
      </div>
    </footer>
  </div>
  <!-- JAVASCRIPTS -->
  <script src="layout/scripts/jquery.min.js"></script>
  <script src="layout/scripts/jquery.backtotop.js"></script>
  <script src="layout/scripts/jquery.mobilemenu.js"></script>
</body>
</html>
