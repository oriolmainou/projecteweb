<?php
  session_start();
  require 'idioma/requirelanguage.php'; // idioma
?>

<!DOCTYPE html>
<html>
<head>
  <title>Bike Tour Barcelona</title>
  <meta charset="utf-8">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <style>
    .videoWrapper {
      position: relative;
      padding-bottom: 56.25%;
      padding-top: 20px;
      height: 0;
    }
    .videoWrapper iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
  </style>
  <script LANGUAGE="JavaScript">
    function abreSitio(){
      var web = document.form1.sitio.options[document.form1.sitio.selectedIndex].value;
      window.open(web);
    }
  </script>
  <meta name="keywords" content="php, multilingüe, multiidioma,website">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>
</head>
<body id="top">
  <div class="wrapper row0">
    <div id="topbar" class="hoc clear"><br>
      <div id="idioma">
        <form name="form1" method="post">
          <select id="idiomas" name="sitio" onChange="javascript:abreSitio()">
            <option><?php echo $idioma ?></option>
            <option value="idioma/changelanguage.php?language=ca">- <?php echo $catala ?></a></option>
            <option value="idioma/changelanguage.php?language=es">- <?php echo $castella ?></a></option>
            <option value="idioma/changelanguage.php?language=en">- <?php echo $angles ?></a></option>
          </select>
        </form>
      </div>
      <div>
        <ul class="nospace inline pushright">
          <li><i class="fa fa-sign-in">&nbsp</i><a href="iniciarRegistrar/iniciar.php" target="_blank"><?php echo $iniciarSessió; ?></a></li>
          <li><i class="fa fa-user">&nbsp</i><a href="iniciarRegistrar/registrar.php" target="_blank"><?php echo $registrar; ?></a></li>
        </ul>
      </div>
    </div>
  </div>
<div class="wrapper row1">
  <header id="header" class="hoc clear">
    <nav id="mainav" class="fl_right">
      <ul class="clear">
        <li><a href="index.php"><?php echo $menu1 ?></a></li>
        <li><a href="rutes.php"><?php echo $menu2 ?></a></li>
        <li><a href="bicicletes.php"><?php echo $menu3 ?></a></li>
        <li><a href="normes.php"><?php echo $menu4 ?></a></li>
        <li><a href="blog.php"><?php echo $menu5 ?></a></li>
        <li><a href="faqs.php"><?php echo $menu6 ?></a></li>
        <li class="active"><a href="contacte.php"><?php echo $menu7 ?></a></li>
      </ul>
    </nav>
  </header>
</div>
<div class="wrapper row3">
  <main class="hoc container clear"><center>
    <h1 id="formContacte"><?php echo $contacteAmbNosaltres; ?></h1>
    <p><?php echo $msg; ?></p>
    <br>
    <form method="post" id="form_Contacte" action="contacte.php">
       <label id="label"><?php echo $noms . ":"; ?></label>
       <input id="nom" name="nom" placeholder="<?php echo $noms ?>" required/>
       <label id="label"><?php echo $email . ":"; ?></label>
       <input id="email" name="email" type="email" placeholder="<?php echo $email ;?>" required>
       <label id="label"><?php echo $missatge . ":"; ?></label>
       <textarea id="missatge" name="missatge" placeholder="<?php echo $escriuAqui; ?>" required></textarea>
       <input id="submit" name="submit" type="submit" value="<?php echo "$formulariEnviar"; ?>">
     </form>
     <?php
        if (isset($_POST['submit'])) {
          $conexion = mysqli_connect("localhost", "root", "", "biketourbarcelona");
          $nom = $_POST['nom'];
          $email = $_POST['email'];
          $missatge = $_POST['missatge'];

          mysqli_query($conexion, "INSERT INTO contacte(nomCognoms, email, missatge) VALUES ('$nom', '$email', '$missatge')");
          mysqli_close($conexion);
        }
     ?>
     <br><br>
     <hr noshade="noshade" style="color: black;">
     <br><br>
     <h3><?php echo $titolUbicacio; ?></h3>
     <p>Plaça del Nord, 14, 08024 Barcelona</p>
     <br>
     <div class="row">
       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2992.472061924887!2d2.1521597153868646!3d41.407270279262356!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a2a4b1b1efe3%3A0x68b69570c52ab5a6!2sPla%C3%A7a+del+Nord%2C+14%2C+08024+Barcelona!5e0!3m2!1ses!2ses!4v1546207945457" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
     </div><br><br>
     <div class="videoWrapper">
       <iframe width="500" height="349" src="images/biketour.mp4" frameborder="0" allowfullscreen></iframe>
     </div>
   </center>
   <br>
  </main>
</div>
<div class="wrapper row4 bgded overlay" style="background-color: #FAFAFA;">
  <footer id="footer" class="hoc clear">
    <div class="one_quarter first">
      <h6 class="title">Bike Tour Barcelona</h6>
      <p><?php echo $descripcio; ?></p>
    </div>
    <div class="one_quarter">
      <h6 class="title"><?php echo $contacteAmbNosaltres; ?></h6>
      <ul class="nospace linklist contact">
        <li><i class="fa fa-map-marker"></i>
          <address>Plaça del Nord 14 <br>08029 Barcelona</address>
        </li>
        <li><i class="fa fa-phone"></i>+34 934.547.411</li>
        <li><i class="fa fa-envelope-o"></i>biketour@bcnbiketour.cat</li>
      </ul>
    </div>
    <div class="one_quarter">
      <h6 class="title"><?php echo $xarxesSocials; ?></h6>
      <ul class="nospace linklist contact">
        <li><i class="fa fa-twitter-square"></i><a style="color: white" href="#" title="Twitter">Twitter</a></li>
        <li><i class="fa fa-facebook-square"></i><a style="color: white" href="#" title="Facebook">Facebook</a></li>
        <li><i class="fa fa-instagram"></i><a style="color: white" href="#" title="Instagram">Instagram</a></li>
      </ul>
    </div>
    <div>
      <?php temps(); ?>
    </div>
  </footer>
</div>
<!-- -->
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>
</body>
</html>
