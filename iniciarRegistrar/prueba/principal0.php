<?php
	session_start();
	require '../../idioma/requirelanguage.php'; // idioma

	if (!isset($_SESSION['usuario'])) {
		header('Location: ../iniciar.php');
	}

	if (isset($_POST['button'])) {
		session_destroy();
		header('Location: ../../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Bike Tour Barcelona</title>
  <meta charset="utf-8">
  <link href="../../layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<meta name="keywords" content="php, multilingüe, multiidioma,website">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>
	<script LANGUAGE="JavaScript">
		function abreSitio(){
			var web = document.form1.sitio.options[document.form1.sitio.selectedIndex].value;
			window.open(web);
		}
	</script>
	<style>
		#buttonMarxar{
			background-color: #008CBA;
			border: black;
			color: white;
			cursor: pointer;
			width: 80px;
			height: 20px;
		}
		#buttonEliminarUsuari{
			background-color: #f44336;
			border: black;
			color: white;
			cursor: pointer;
			width: 120px;
			height: 20px;
		}
		.card {
		  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
		  border-radius: 50px;
		  width: 60%;
			background-color: #EEEEEE;
		}
		.container {
  		padding: 2px 16px;
		}
		.card2 {
		  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
		  transition: 0.3s;
		  width: 60%;
			background-color: #F9F9F9;
		}
		.open-button {
		  background-color: #555;
		  color: white;
		  padding: 16px;
		  border: none;
		  cursor: pointer;
		  width: 180px;
		}
		.form-popup {
		  display: none;
		  position: fixed;
		  bottom: 0;
		  right: 15px;
		  border: 3px solid #f1f1f1;
		  z-index: 9;
		}
		.form-container {
		  max-width: 300px;
		  padding: 10px;
		  background-color: white;
		}
		.form-container input[type=text], .form-container input[type=password] {
		  width: 100%;
		  padding: 15px;
		  margin: 5px 0 22px 0;
		  border: none;
		}
		.form-container .btn {
		  background-color: #4CAF50;
		  color: white;
		  border: none;
		  cursor: pointer;
		  margin-bottom:10px;
		}
		.form-container .cancel {
		  background-color: red;
		}
	</style>
</head>
<body id="top">
	<?php
		if (isset($_GET['err'])) {
			echo "<script>alert('$dataMal')</script>";
		}
	?>
  <div class="wrapper row0">
    <div id="topbar" class="hoc clear"><br>
			<div id="idioma">
        <form name="form1" method="post">
          <select id="idiomas" name="sitio" onChange="javascript:abreSitio()">
						<option><?php echo $idioma ?></option>
						<option value="idioma/changelanguage.php?language=ca">- <?php echo $catala ?></a></option>
            <option value="idioma/changelanguage.php?language=es">- <?php echo $castella ?></a></option>
            <option value="idioma/changelanguage.php?language=en">- <?php echo $angles ?></a></option>
          </select>
        </form>
      </div>
      <div>
        <ul class="nospace inline pushright">
          <li>
						<i class="fa fa-sign-in"></i>
						<form method="POST">
							<button type="submit" id="buttonMarxar" name="button"><?php echo $marxar; ?></button>
						</form>
						<form method="POST">
							<button type="submit" id="buttonEliminarUsuari" name="buttonEliminarUsuari"><?php echo $eliminarCompte; ?></button>
						</form>

						<?php
							if (isset($_POST['buttonEliminarUsuari'])) {
								$usuari2 = "root";
								$contrasenya2 = "";
								$servidor2 = "localhost";
								$basededades2 = "biketourbarcelona";

								$conexion2 = mysqli_connect($servidor2, $usuari2, "") or die ("No se ha conectado");
								$db2 = mysqli_select_db($conexion2, $basededades2) or die ("No se ha conectado");
								$consulta2 = "DELETE FROM persona WHERE email = " . "'". $_SESSION['email']. "'";
								$resultado2 = mysqli_query($conexion2, $consulta2) or die ("No se ha hecho la consulta");
								mysqli_close($conexion2);

								session_destroy();
								header('Location: ../iniciar.php');
							}
						?>
					</li>
        </ul>
      </div>
    </div>
		<br>
  </div>
	<div class="wrapper row3">
	  <main class="hoc container clear"><center><br>
	    <h1 id="dadesContacto"><?php echo $titol_dadesUsuaris; ?></h1>
			<div class="card">
				<br>
					<?php
						$servername = "localhost";
						$username = "root";
						$password = "";
						$dbname = "biketourbarcelona";

						$conn = mysqli_connect($servername, $username, $password, $dbname);
						$idUser = $_SESSION['usuario'];
						$sql = "SELECT * FROM persona WHERE email = '$idUser'";
						$result = mysqli_query($conn, $sql);

						if (mysqli_num_rows($result) == true) {
							$row = mysqli_fetch_assoc($result);
							$sexeHome = $row["sexe"];
							if ($sexeHome == "home") {
								?>
									<img src="../../images/img_avatar.png" title="<?php echo $benvingut . " " . $_SESSION['nom'] . "!";?>" style="width:30%">
								<?php
							} else {
								?>
									<img src="../../images/img_avatar_mujer.png" title="<?php echo $benvinguda . " " . $_SESSION['nom'] . "!";?>" style="width:30%">
								<?php
							}
						}
					?>
				<br><br>
	  		<div class="container" style="text-align: center">
		    	<h1><b>
						<?php echo $_SESSION['nom'] . " " . $_SESSION['cognoms']; ?>
					</b></h1>
					<p> <?php echo $_SESSION['email']; ?></p>
					<p> <?php echo $_SESSION['mobil']; ?></p><br>

					<center>
						<button class="open-button" onclick="openForm()"><?php echo $editarPwd; ?></button>
					</center>
					<div class="form-popup" id="myForm">
					  <form method="post" action="principal.php" class="form-container">
					    <label for="psw"><b>Nova contrasenya</b></label>
					    <input type="password" placeholder="Contrasenya" name="psw">
					    <button type="submit" name="actualizar" class="btn">Actualitzar</button>
					    <button type="button" class="btn cancel" onclick="closeForm()">Tancar</button>
					  </form>
						<?php

							if (isset($_POST["actualizar"])) {
			            $usuari2 = "root";
			            $contrasenya2 = "";
			            $servidor2 = "localhost";
			            $basededades2 = "biketourbarcelona";
			            $conexion2 = mysqli_connect($servidor2, $usuari2, "") or die ("No se ha conectado");
			            $db2 = mysqli_select_db($conexion2, $basededades2) or die ("No se ha conectado");

			            $encript = md5($_POST["psw"]);

			            $consulta1 = "UPDATE persona SET contrasenya='" . $encript . "'  WHERE email='" . $_SESSION["email"]."'";
			            $return = mysqli_query($conexion2,$consulta1);
							}
						?>
					</div>
					<script>
						function openForm() {
						  document.getElementById("myForm").style.display = "block";
						}

						function closeForm() {
						  document.getElementById("myForm").style.display = "none";
						}
					</script>
					<br>

					<table>
					  <tr>
					    <th><?php echo $bicisReservades; ?></th>
					    <th><?php echo $dataReserva; ?></th>
					  </tr>
					  <tr>
					    <td>
								<?php
									$correoReservadesBicis = $_SESSION['email'];
									$sqls = "SELECT reservades FROM bicicletes WHERE emailUsuariRegistrat = '$correoReservadesBicis'";
									$result = mysqli_query($conn, $sqls);

									if (mysqli_num_rows($result) > 0) {
										while($row = mysqli_fetch_assoc($result)) {
											echo $row["reservades"] . "<br><br>";
										}
									}
								?>
							</td>

					    <td>
								<?php
									$correoReservadesBicis = $_SESSION['email'];
									$sqls = "SELECT diaHoraReserva FROM bicicletes WHERE emailUsuariRegistrat = '$correoReservadesBicis'";
									$result = mysqli_query($conn, $sqls);

									if (mysqli_num_rows($result)) {
										while($row = mysqli_fetch_assoc($result)) {
											echo $row["diaHoraReserva"];
											echo "<br><br>";
										}
									}
								?>
							</td>
					  </tr>
					</table>
					<?php //mysqli_close($conn); ?>
					<br>
	  		</div>
			</div>
			<br><hr><br>
			<h1 id="reservarBicis"><?php echo $titolReservar; ?></h1>
			<p><?php echo $txt; ?></p><br>
			<div class="card2">
		    <form method="post" id="form_Contacte" action="principal.php"><br>
					 <label id="label"><?php echo $bicisAReservar; ?>:</label>
					 <input type="number" name="biciReservar">
		       <label id="label"><?php echo $dataReserva; ?>:</label>
					 <input type="datetime-local" name="diaHora">
					 <label id="label"><?php echo $tipus; ?>:</label>
					 <select name="electrica" required>
						 <option value=""><?php echo $opcioTriar; ?></option>
						 <option>Electrica</option>
						 <option>Tradicional</option>
					 </select>
					 <label id="label"><?php echo $menu2; ?>:</label>
					 <select name="opcionRuta" required>
						 <option value=""><?php echo $opcioTriar; ?></option>
						 <option>Fer la meva propia ruta</option>
						 <?php
						 $sqls = "SELECT titol FROM routes";
						 $result = mysqli_query($conn, $sqls);

						 if (mysqli_num_rows($result) > 0) {
							 while($row = mysqli_fetch_assoc($result)) {
								 echo "<option>" . $row["titol"] . "</option>";
							 }
						 }
						 ?>
					 </select>
					 <br>
		       <input id="submit" name="submit" type="submit" value="<?php echo $formulariEnviar; ?>">
					 <br>
		     </form>
				 <br>
			 </div>
			 <?php
	        if (isset($_POST['submit'])) {
						$id = 1;
						require('conexion.php');
						$today = new DateTime("now");
						$fecha2 = new DateTime($_POST['diaHora']);
						if($today > $fecha2){
							header("Location: principal.php?err=Data incorrecte");
						}

						$consulta = $conexion->prepare("SELECT disponibles FROM bicicletes WHERE diaHoraReserva<='".$_POST["diaHora"]."'");
						$consulta->execute();

						while ($row = $consulta->fetch()) {
					    $disponibles = $row['disponibles'];
						}

						if ($_POST['biciReservar'] > $disponibles) {
							echo "<script>alert('Hi ha masses bicicletes que vols reservar.')</script>";
						} else {
							echo "<script language='JavaScript'>location.href = 'bicicletesJaReservades.php' </script>";

							$conexion = mysqli_connect("localhost", "root", "", "biketourbarcelona");
		          $nom = $_POST['nom'];
		          $email = $_POST['email'];
		          $biciReservar = $_POST['biciReservar'];
							$electrica = $_POST['electrica'];
							$diaHora = $_POST['diaHora'];
							$opcionRuta = $_POST['opcionRuta'];
							$disponible = $disponibles - $_POST['biciReservar'];
							$emailRegistrat = $_SESSION['email'];

		          mysqli_query($conexion,"INSERT INTO bicicletes(reservades,disponibles,diaHoraReserva,ruta,emailUsuariRegistrat,tipus)
								VALUES('$biciReservar', $disponible, '$diaHora', '$opcionRuta', '$emailRegistrat', '$electrica')");
							mysqli_close($conexion);
						}
	        }
	     ?>
			 <br><hr><br>
			 <h1 id="blog"><?php echo $blogEmpresa; ?></h1>
			 <p><?php echo $comentariBlog; ?></p>
			 <br>
			 <form method="post" id="form_Contacte" action="principal.php">
				 <textarea id="missatgeBlog" name="missatgeBlog" placeholder="<?php echo $escriuAqui; ?>" required></textarea>
				 <input id="submit" name="submits" type="submit" value="<?php echo $formulariEnviar; ?>">
					<?php
						if (isset($_POST['submits'])) {
							$con = mysqli_connect("localhost", "root", "", "biketourbarcelona");
							$texts = $_POST['missatgeBlog'];
							$nomBlog = $_SESSION['nom'];
							$data = date('Y-m-d');
							mysqli_query($con,"INSERT INTO blogEmpresa(nom,texts,data) VALUES('$nomBlog', '$texts','$data')");
							mysqli_close($con);
						}
					?>
			 </form>
	   <br><br>
	  </main>
	</div>
	<div class="wrapper row4 bgded overlay" style="background-color: #FAFAFA;">
		<footer id="footer" class="hoc clear">
      <div class="one_quarter first">
        <h6 class="title">Bike Tour Barcelona</h6>
        <p><?php echo $descripcio; ?></p>
      </div>
      <div class="one_quarter">
        <h6 class="title"><?php echo $contacteAmbNosaltres; ?></h6>
        <ul class="nospace linklist contact">
          <li><i class="fa fa-map-marker"></i>
            <address>Plaça del Nord 14 <br>08029 Barcelona</address>
          </li>
          <li><i class="fa fa-phone"></i>+34 934.547.411</li>
          <li><i class="fa fa-envelope-o"></i>biketour@bcnbiketour.cat</li>
        </ul>
      </div>
      <div class="one_quarter">
        <h6 class="title"><?php echo $xarxesSocials; ?></h6>
        <ul class="nospace linklist contact">
          <li><i class="fa fa-twitter-square"></i><a style="color: white" href="#" title="Twitter">Twitter</a></li>
          <li><i class="fa fa-facebook-square"></i><a style="color: white" href="#" title="Facebook">Facebook</a></li>
          <li><i class="fa fa-instagram"></i><a style="color: white" href="#" title="Instagram">Instagram</a></li>
        </ul>
      </div>
      <div>
				<?php temps(); ?>
      </div>
    </footer>
	</div>
	<!-- -->
	<script src="layout/scripts/jquery.min.js"></script>
	<script src="layout/scripts/jquery.backtotop.js"></script>
	<script src="layout/scripts/jquery.mobilemenu.js"></script>
</body>
</html>
