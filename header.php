<?php

  function topbar(){
    ?>
    <script LANGUAGE="JavaScript">
      function abreSitio(){
        var web = document.form1.sitio.options[document.form1.sitio.selectedIndex].value;
        window.open(web);
      }
    </script>
    <div class="wrapper row0">
      <div id="topbar" class="hoc clear"><br>
        <div id="idioma">
          <form name="form1" method="post">
            <select id="idiomas" name="sitio" onChange="javascript:abreSitio()">
              <option value=""></option>
              <option value="http://localhost:8080/M12-Projecte-Oriol_Mainou/Projecte-Web/catala/index.php">Català</option>
              <option value="http://localhost:8080/M12-Projecte-Oriol_Mainou/Projecte-Web/castella/index.php">Castellano</option>
              <option value="http://localhost:8080/M12-Projecte-Oriol_Mainou/Projecte-Web/angles/index.php">English</option>
            </select>
          </form>
        </div>
          <ul class="nospace inline pushright">
            <li><i class="fa fa-sign-in">&nbsp</i><a href="iniciarRegistrar/iniciar.php" target="_blank"><?php echo $iniciarSessió; ?></a></li>
            <li><i class="fa fa-user">&nbsp</i><a href="iniciarRegistrar/registrar.php" target="_blank"><?php echo $registrar; ?></a></li>
          </ul>
        </div>
      </div>
      <?php
  }

  function footer(){
    ?>
    <div class="wrapper row4 bgded overlay" style="background-color: #FAFAFA;">
      <footer id="footer" class="hoc clear">
        <div class="one_quarter first">
          <h6 class="title">Bike Tour Barcelona</h6>
          <p>Som el servei de lloguer de bicicletes líder de Barcelona amb bicicletes tant elèctiques com tradicionals. Coneix la ciutat amb les nostres rutes i paga per hores d'ús.</p>
        </div>
        <div class="one_quarter">
          <h6 class="title">Contacte amb nosaltres</h6>
          <ul class="nospace linklist contact">
            <li><i class="fa fa-map-marker"></i>
              <address>Plaça del Nord 14 <br>08029 Barcelona</address>
            </li>
            <li><i class="fa fa-phone"></i>+34 934.547.411</li>
            <li><i class="fa fa-envelope-o"></i>biketour@bcnbiketour.cat</li>
          </ul>
        </div>
        <div class="one_quarter">
          <h6 class="title">Xarxes socials</h6>
          <ul class="nospace linklist contact">
            <li><i class="fa fa-twitter-square"></i><a style="color: white" href="#" title="Twitter">Twitter</a></li>
            <li><i class="fa fa-facebook-square"></i><a style="color: white" href="#" title="Facebook">Facebook</a></li>
            <li><i class="fa fa-instagram"></i><a style="color: white" href="#" title="Instagram">Instagram</a></li>
          </ul>
        </div>
        <div>
          <?php temps(); ?>
        </div>
      </footer>
    </div>
    <?php
  }



?>
