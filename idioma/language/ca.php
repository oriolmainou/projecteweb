<?php
  $iniciarSessió = "Iniciar sessió";
  $registrar = "Registrar-se";
  $catala = "Català";
  $castella = "Castellano";
  $angles = "English";
  $idioma = "Idioma";
  $menu1 = "Pàgina principal";
  $menu2 = "Rutes";
  $menu3 = "Bicicletes";
  $menu4 = "Normes";
  $menu5 = "Blog";
  $menu6 = "Faqs";
  $menu7 = "Contacte";
  $descripcio = "Som el servei de lloguer de bicicletes líder de Barcelona amb bicicletes tant elèctiques com tradicionals. Coneix la ciutat amb les nostres rutes i paga per hores d'ús.";
  $rutesPrincipalsTitol = "Les nostres rutes principals";
  $veureRuta= "Clica per veure la ruta";
  $sqlSentencia1 = "SELECT descripcio FROM routes WHERE id=1";
  $descripcioSentencia = "descripcio";
  $sqlSentenciaPreu1 = "SELECT preus FROM routes WHERE id=1";
  $preu = "Preu";
  $persona = "Persona";
  $sqlSentencia2 = "SELECT descripcio FROM routes WHERE id=2";
  $descripcioSentencia2 = "descripcio";
  $sqlSentencia3 = "SELECT descripcio FROM routes WHERE id=3";
  $descripcioSentencia3 = "descripcio";
  $titolUbicacio = "On estem ubicats?";
  $descripcioUbicacio = "Estem ubicats al Barri de Gràcia de Barcelona. Les línies de metro a prop que tenim són: L3 (Lesseps i Fontana) i L4 (Joanic), més unes amplies línies d'autobusos (22, 24, 27, 28, 31, 32, 39, 55, 74, 87, 92, 114, 116).";
  $contacteAmbNosaltres = "Contacte amb nosaltres";
  $xarxesSocials = "Xarxes socials";
  $sqlSentencia4 = "SELECT descripcio FROM routes WHERE id=4";
  $descripcioSentencia4 = "descripcio";
  $sqlSentencia5 = "SELECT descripcio FROM routes WHERE id=5";
  $descripcioSentencia5 = "descripcio";
  $sqlSentencia6 = "SELECT descripcio FROM routes WHERE id=6";
  $descripcioSentencia6 = "descripcio";
  $sqlSentencia7 = "SELECT descripcio FROM routes WHERE id=7";
  $descripcioSentencia7 = "descripcio";
  $sqlSentencia8 = "SELECT descripcio FROM routes WHERE id=8";
  $descripcioSentencia8 = "descripcio";
  $lesNostresRutes = "Les nostres rutes";
  $sqlSentencia9 = "SELECT descripcio FROM routes WHERE id=9";
  $descripcioSentencia9 = "descripcio";
  $sqlSentencia10 = "SELECT descripcio FROM routes WHERE id=10";
  $descripcioSentencia10 = "descripcio";
  $sqlSentencia11 = "SELECT descripcio FROM routes WHERE id=11";
  $descripcioSentencia11 = "descripcio";
  $seguent = "Següent";
  $anterior = "Anterior";
  $sqlSentencia12 = "SELECT descripcio FROM routes WHERE id=12";
  $descripcioSentencia12 = "descripcio";
  $sqlSentencia13 = "SELECT descripcio FROM routes WHERE id=13";
  $descripcioSentencia13 = "descripcio";
  $sqlSentencia14 = "SELECT descripcio FROM routes WHERE id=14";
  $descripcioSentencia14 = "descripcio";
  $sqlSentencia15 = "SELECT descripcio FROM routes WHERE id=15";
  $descripcioSentencia15 = "descripcio";
  $sqlSentencia16 = "SELECT descripcio FROM routes WHERE id=16";
  $descripcioSentencia16 = "descripcio";
  $sqlSentencia17 = "SELECT descripcio FROM routes WHERE id=17";
  $descripcioSentencia17 = "descripcio";
  $sqlSentencia18 = "SELECT descripcio FROM routes WHERE id=18";
  $descripcioSentencia18 = "descripcio";
  $sqlSentencia19 = "SELECT descripcio FROM routes WHERE id=19";
  $descripcioSentencia19 = "descripcio";
  $sqlSentencia20 = "SELECT descripcio FROM routes WHERE id=20";
  $descripcioSentencia20 = "descripcio";
  $sqlSentencia21 = "SELECT descripcio FROM routes WHERE id=21";
  $descripcioSentencia21 = "descripcio";
  $sqlSentencia22 = "SELECT descripcio FROM routes WHERE id=22";
  $descripcioSentencia22 = "descripcio";
  $bicicletesNostres = "Les nostres bicicletes";
  $bicis1 = "Tenim bicicletes per a totes les edats. Tenim bicicletes elèctriques i bicicletes tradicionals. En el moment de llogar un tour hi ha la possibilitat de escollir bicicleta.";
  $bicis2 = "Registra't i gaudeix del teu tour preferit amb tota la família i amics.";
  $bicicletaTradicional = "Bicicleta tradicional";
  $bicicletaElectrica = "Bicicleta electrica";
  $normes = "NORMES";
  $descrip = "descripcio";
  $titolBlog = "Blog de l'empresa";
  $titol = "titol";
  $formulariEnviar = "Enviar";
  $escriuAqui = "Escriu-nos aqui...";
  $missatge = "Missatge de contacte";
  $email = "Correu electrònic";
  $noms = "Nom i cognoms";
  $msg = "Envia'ns el problema i et contestarem el més aviat possible.";

  // INICIAR SESSIO

  $contrasenya = "Contrasenya";
  $entrar = "Entrar";
  $oblidacio = "No recordes la contrasenya? Entra";
  $aqui = "aquí";

  // FORM OBLIDAR

  $Recuperarcontrasenya = "Recuperar contrasenya";
  $canviarContrasenya = "Canviar contrasenya";
  $tornarIniciSessio = "Tornar a iniciar sessió";

  // BICICLETES JA RESERVADES

  $reservaFeta = "Reserva feta correctament";
  $tornar = "Tornar";

  // USUARI

  $dataMal = "Data incorrecte";
  $marxar = "Tancar";
  $eliminarCompte = "Eliminar compte";
  $titol_dadesUsuaris = "DADES DE CONTACTE";
  $editarPwd = "Editar contrasenya";
  $bicisReservades = "Bicicletes reservades";
  $dataReserva = "Data de la reserva";
  $ruta = "Ruta";
  $titolReservar = "RESERVAR BICICLETES";
  $txt = "Indica el dia i hora per reservar les bicicletes. Ens posarem en contacte.";
  $bicisAReservar = "Bicicletes a reservar";
  $tipus = "Tipus";
  $opcioTriar = "Tria una opció...";
  $blogEmpresa = "BLOG DE L'EMPRESA";
  $blogMissatgeEnviat = "Missatge enviat.";
  $bicisJaReservades = "Número de bicicletes:";
  $comentariBlog = "Escriu un comentari referent a l'empresa. El teu comentari pot ajudar-nos. <br> Els missatges es publicaran a l'apartat 'Blog' de la pàgina principal.";

  // REGISTRAR

  $nom = "Nom";
  $cognoms = "Cognoms";
  $sexe = "Sexe";
  $ciutat = "Ciutat";
  $pais = "Pais";
  $DNI = "DNI/Passaport";
  $mobil = "Mòbil";
  $naixement = "Data de naixement";
  $targeta = "Targeta bancària";
  $tornarPagina = "Tornar a la pàgina";
  $benvingut = "Benvingut";
  $benvinguda = "Benvinguda";

  // FOOTER TEMPS

  function temps(){
    ?>
      <div id="c_de3354e2d05aa74e5727d74fe3d1addc" class="normal"></div><script type="text/javascript" src="https://ca.eltiempo.es/widget/widget_loader/de3354e2d05aa74e5727d74fe3d1addc"></script>
    <?php
  }
  $paginaUsuari = "Pàgina d'usuari";

?>
