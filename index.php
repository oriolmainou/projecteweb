<?php
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "biketourbarcelona";
  $conn = new mysqli($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }
  $conn->set_charset("utf8");
  session_start();
  require 'idioma/requirelanguage.php'; // idioma
?>

<!DOCTYPE html>
<html>
<head>
  <title>Bike Tour Barcelona</title>
  <meta charset="UTF-8">
  <link href="layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
  <meta name="keywords" content="php, multilingüe, multiidioma,website">
  <script LANGUAGE="JavaScript">
    function abreSitio(){
      var web = document.form1.sitio.options[document.form1.sitio.selectedIndex].value;
      window.open(web);
    }
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js" integrity="sha384-vZ2WRJMwsjRMW/8U7i6PWi6AlO1L79snBrmgiDpgIWJ82z8eA5lenwvxbMV1PAh7" crossorigin="anonymous"></script>
</head>
<body id="top">
  <div class="wrapper row0">
    <div id="topbar" class="hoc clear"><br>
      <div id="idioma">
        <form name="form1" method="post">
          <select id="idiomas" name="sitio" onChange="javascript:abreSitio()">
            <option><?php echo $idioma ?></option>
            <option value="idioma/changelanguage.php?language=ca">- <?php echo $catala ?></a></option>
            <option value="idioma/changelanguage.php?language=es">- <?php echo $castella ?></a></option>
            <option value="idioma/changelanguage.php?language=en">- <?php echo $angles ?></a></option>
          </select>
        </form>
      </div>
      <div>
        <ul class="nospace inline pushright">
          <li><i class="fa fa-sign-in">&nbsp</i><a href="iniciarRegistrar/iniciar.php" target="_blank"><?php echo $iniciarSessió; ?></a></li>
          <li><i class="fa fa-user">&nbsp</i><a href="iniciarRegistrar/registrar.php" target="_blank"><?php echo $registrar; ?></a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="wrapper row1">
    <header id="header" class="hoc clear">
      <nav id="mainav" class="fl_right">
        <ul class="clear">
          <li class="active"><a href="index.php"><?php echo $menu1 ?></a></li>
          <li><a href="rutes.php"><?php echo $menu2 ?></a></li>
          <li><a href="bicicletes.php"><?php echo $menu3 ?></a></li>
          <li><a href="normes.php"><?php echo $menu4 ?></a></li>
          <li><a href="blog.php"><?php echo $menu5 ?></a></li>
          <li><a href="faqs.php"><?php echo $menu6 ?></a></li>
          <li><a href="contacte.php"><?php echo $menu7 ?></a></li>
        </ul>
      </nav>
    </header>
  </div>
  <div class="wrapper bgded overlay" style="background-image:url('images/barcelona.jpg');">
    <div id="pageintro" class="hoc clear">
      <article>
        <h2 class="heading">Bike Tour Barcelona</h2> <br>
        <p><?php echo $descripcio; ?></p>
      </article>
    </div>
  </div>
  <div class="wrapper row3">
    <main class="hoc container clear">
      <div class="center btmspace-50">
        <h1><b><?php echo $rutesPrincipalsTitol; ?></b></h1>
      </div>
      <ul class="nospace group btmspace-50">
        <li class="one_third first">
          <article class="element">
            <figure>
              <a target="_blank" href="https://www.google.es/maps/dir/41.3630926,2.1571632/41.3619214,2.1527031/@41.3617986,2.1524198,15.25z/data=!4m14!4m13!1m10!3m4!1m2!1d2.1683571!2d41.3673291!3s0x12a4a243bfcfc68f:0x3604dfc5ac953c31!3m4!1m2!1d2.1480002!2d41.3657951!3s0x12a4a274264febc5:0x8bf847fded63ebe9!1m0!3e1">
                <img src="images/estadiolimpic.jpg" title="<?php echo $veureRuta; ?>">
              </a>
            </figure>
            <div class="excerpt">
              <h6 class="heading">
                <?php
                  $sql = "SELECT titol FROM routes WHERE id=1";
                  $result = $conn->query($sql);
                  if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                      echo $row["titol"];
                    }
                  }
                ?>
              </h6>
              <p>
                <?php
                  $sql = "$sqlSentencia1";
                  $result = $conn->query($sql);
                  if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                      echo $row[$descripcioSentencia];
                    }
                  }
                ?>
              </p>
              <p><b><?php
                $sql = "$sqlSentenciaPreu1";
                $result = $conn->query($sql);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    echo "$preu: " . $row["preus"] . "€/$persona.";
                  }
                }
              ?></b></p>
            </div>
          </article>
        </li>
        <li class="one_third">
          <article class="element">
            <figure>
              <a target="_blank" href="https://www.google.es/maps/dir/Pl.+Catalunya,+08002+Barcelona/Casa+Batll%C3%B3,+Passeig+de+Gr%C3%A0cia,+Barcelona/@41.3939301,2.1664622,14.96z/data=!4m34!4m33!1m25!1m1!1s0x12a4a2f3c37acd4f:0xc6878e918e86b13e!2m2!1d2.1696958!2d41.38736!3m4!1m2!1d2.1760952!2d41.3843334!3s0x12a4a2f9bd82c8f5:0xfa17ea32fbdb1c83!3m4!1m2!1d2.1828646!2d41.3887574!3s0x12a4a2fd11050265:0x951ee825178096ad!3m4!1m2!1d2.1751525!2d41.403966!3s0x12a4a2dcc4d59813:0xc106617410c76dd6!3m4!1m2!1d2.1620418!2d41.3951635!3s0x12a4a2939d0c6229:0xb9ba4e3156f872d7!1m5!1m1!1s0x12a4a2ed494b4161:0x40d2782f9e2e4e0f!2m2!1d2.1647698!2d41.3916384!3e1">
                <img src="images/plcatalunya.jpg" title="<?php echo $veureRuta; ?>">
              </a>
            </figure>
            <div class="excerpt">
              <h6 class="heading">
                <?php
                  $sql = "SELECT titol FROM routes WHERE id=2";
                  $result = $conn->query($sql);
                  if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                      echo $row["titol"];
                    }
                  }
                ?>
              </h6>
              <p>
                <?php
                  $sql = "$sqlSentencia2";
                  $result = $conn->query($sql);
                  if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                      echo $row[$descripcioSentencia];
                    }
                  }
                ?>
              </p>
              <p><b><?php
                $sql = "SELECT preus FROM routes WHERE id=2";
                $result = $conn->query($sql);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    echo "$preu: " . $row["preus"] . "€/$persona.";
                  }
                }
              ?></b></p>
            </div>
          </article>
        </li>
        <li class="one_third">
          <article class="element">
            <figure>
              <a target="_blank" href="https://www.google.es/maps/dir/Parc+del+Guinard%C3%B3,+Barcelona/41.4199117,2.1661906/41.4121191,2.1527023/@41.4158968,2.1554915,16z/data=!4m15!4m14!1m5!1m1!1s0x12a4a2cce8993c0f:0x48033cdca93fca37!2m2!1d2.167375!2d41.4194667!1m5!3m4!1m2!1d2.1587669!2d41.413153!3s0x12a4a2b0db454cf9:0x109c0fde2594871c!1m0!3e1">
                <img src="images/pguell.jpg" title="<?php echo $veureRuta; ?>">
              </a>
            </figure>
            <div class="excerpt">
              <h6 class="heading">
                <?php
                  $sql = "SELECT titol FROM routes WHERE id=3";
                  $result = $conn->query($sql);
                  if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                      echo $row["titol"];
                    }
                  }
                ?>
              </h6>
              <p>
                <?php
                  $sql = "$sqlSentencia3";
                  $result = $conn->query($sql);
                  if (mysqli_num_rows($result) > 0) {
                    while($row = mysqli_fetch_assoc($result)) {
                      echo $row["$descripcioSentencia3"];
                    }
                  }
                ?>
              </p>
              <p><b><?php
                $sql = "SELECT preus FROM routes WHERE id=3";
                $result = $conn->query($sql);
                if (mysqli_num_rows($result) > 0) {
                  while($row = mysqli_fetch_assoc($result)) {
                    echo "$preu: " . $row["preus"] . "€/$persona.";
                  }
                }
              ?></b></p>
            </div>
          </article>
        </li>
      </ul>
    </main>
  </div>
  <div class="wrapper bgded overlay coloured" style="background-color: #5E5FF0;">
    <div class="hoc container clear">
      <article class="center">
        <h2 class="font-x3"><?php echo $titolUbicacio; ?></h2>
        <p><?php echo $descripcioUbicacio; ?></p><br>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2992.4720619248874!2d2.152159715426404!3d41.40727027926234!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a2a4b1b1efe3%3A0x68b69570c52ab5a6!2sPla%C3%A7a+del+Nord%2C+14%2C+08024+Barcelona!5e0!3m2!1sca!2ses!4v1557147927762!5m2!1sca!2ses" width="800" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
      </article>
    </div>
  </div>
  <div class="wrapper row4 bgded overlay" style="background-color: #FAFAFA;">
    <footer id="footer" class="hoc clear">
      <div class="one_quarter first">
        <h6 class="title">Bike Tour Barcelona</h6>
        <p><?php echo $descripcio; ?></p>
      </div>
      <div class="one_quarter">
        <h6 class="title"><?php echo $contacteAmbNosaltres; ?></h6>
        <ul class="nospace linklist contact">
          <li><i class="fa fa-map-marker"></i>
            <address>Plaça del Nord 14 <br>08029 Barcelona</address>
          </li>
          <li><i class="fa fa-phone"></i>+34 934.547.411</li>
          <li><i class="fa fa-envelope-o"></i>biketour@bcnbiketour.cat</li>
        </ul>
      </div>
      <div class="one_quarter">
        <h6 class="title"><?php echo $xarxesSocials; ?></h6>
        <ul class="nospace linklist contact">
          <li><i class="fa fa-twitter-square"></i><a style="color: white" href="#" title="Twitter">Twitter</a></li>
          <li><i class="fa fa-facebook-square"></i><a style="color: white" href="#" title="Facebook">Facebook</a></li>
          <li><i class="fa fa-instagram"></i><a style="color: white" href="#" title="Instagram">Instagram</a></li>
        </ul>
      </div>
      <div>
        <?php temps(); ?>
      </div>
    </footer>
  </div>
  <!-- JAVASCRIPTS -->
  <script src="layout/scripts/jquery.min.js"></script>
  <script src="layout/scripts/jquery.backtotop.js"></script>
  <script src="layout/scripts/jquery.mobilemenu.js"></script>
  <!-- <script src="layout/scripts/jquery.placeholder.min.js"></script> -->
</body>
</html>
