<?php
  $iniciarSessió = "Iniciar sesión";
  $registrar = "Registrar";
  $catala = "Català";
  $castella = "Castellano";
  $angles = "English";
  $idioma = "Idioma";
  $menu1 = "Pagina principal";
  $menu2 = "Rutas";
  $menu3 = "Bicicletas";
  $menu4 = "Normas";
  $menu5 = "Blog";
  $menu6 = "Faqs";
  $menu7 = "Contacto";
  $descripcio = "Somos el servicio de alquiler de bicicletes líder de Barcelona con bicicletas tanto electricas como tradicionales. Conoce la ciudad con nuestras rutas y paga por horas de uso.";
  $rutesPrincipalsTitol = "Nuestras rutas principales";
  $veureRuta= "Clica para ver la ruta";
  $sqlSentencia1 = "SELECT descripcioCastella FROM routes WHERE id=1";
  $descripcioSentencia = "descripcioCastella";
  $sqlSentenciaPreu1 = "SELECT preus FROM routes WHERE id=1";
  $preu = "Precio";
  $persona = "Persona";
  $sqlSentencia2 = "SELECT descripcioCastella FROM routes WHERE id=2";
  $descripcioSentencia2 = "descripcioCastella";
  $sqlSentencia3 = "SELECT descripcioCastella FROM routes WHERE id=3";
  $descripcioSentencia3 = "descripcioCastella";
  $titolUbicacio = "¿Dónde estamos ubicados?";
  $descripcioUbicacio = "Estamos ubicados en el Barrio de Gràcia de Barcelona. Las lineas de metro más cercanas son: L3 (Lesseps y Fontana) y L4 (Joanic), más unas amplias linias de autobuses (22, 24, 27, 28, 31, 32, 39, 55, 74, 87, 92, 114, 116).";
  $contacteAmbNosaltres = "Contacta con nosotros";
  $xarxesSocials = "Redes sociales";
  $sqlSentencia4 = "SELECT descripcioCastella FROM routes WHERE id=4";
  $descripcioSentencia4 = "descripcioCastella";
  $sqlSentencia5 = "SELECT descripcioCastella FROM routes WHERE id=5";
  $descripcioSentencia5 = "descripcioCastella";
  $sqlSentencia6 = "SELECT descripcioCastella FROM routes WHERE id=6";
  $descripcioSentencia6 = "descripcioCastella";
  $sqlSentencia7 = "SELECT descripcioCastella FROM routes WHERE id=7";
  $descripcioSentencia7 = "descripcioCastella";
  $sqlSentencia8 = "SELECT descripcioCastella FROM routes WHERE id=8";
  $descripcioSentencia8 = "descripcioCastella";
  $lesNostresRutes = "Nuestras rutas";
  $sqlSentencia9 = "SELECT descripcioCastella FROM routes WHERE id=9";
  $descripcioSentencia9 = "descripcioCastella";
  $sqlSentencia10 = "SELECT descripcioCastella FROM routes WHERE id=10";
  $descripcioSentencia10 = "descripcioCastella";
  $sqlSentencia11 = "SELECT descripcioCastella FROM routes WHERE id=11";
  $descripcioSentencia11 = "descripcioCastella";
  $seguent = "Siguiente";
  $anterior = "Anterior";
  $sqlSentencia12 = "SELECT descripcioCastella FROM routes WHERE id=12";
  $descripcioSentencia12 = "descripcioCastella";
  $sqlSentencia13 = "SELECT descripcioCastella FROM routes WHERE id=13";
  $descripcioSentencia13 = "descripcioCastella";
  $sqlSentencia14 = "SELECT descripcioCastella FROM routes WHERE id=14";
  $descripcioSentencia14 = "descripcioCastella";
  $sqlSentencia15 = "SELECT descripcioCastella FROM routes WHERE id=15";
  $descripcioSentencia15 = "descripcioCastella";
  $sqlSentencia16 = "SELECT descripcioCastella FROM routes WHERE id=16";
  $descripcioSentencia16 = "descripcioCastella";
  $sqlSentencia17 = "SELECT descripcioCastella FROM routes WHERE id=17";
  $descripcioSentencia17 = "descripcioCastella";
  $sqlSentencia18 = "SELECT descripcioCastella FROM routes WHERE id=18";
  $descripcioSentencia18 = "descripcioCastella";
  $sqlSentencia19 = "SELECT descripcioCastella FROM routes WHERE id=19";
  $descripcioSentencia19 = "descripcioCastella";
  $sqlSentencia20 = "SELECT descripcioCastella FROM routes WHERE id=20";
  $descripcioSentencia20 = "descripcioCastella";
  $sqlSentencia21 = "SELECT descripcioCastella FROM routes WHERE id=21";
  $descripcioSentencia21 = "descripcioCastella";
  $sqlSentencia22 = "SELECT descripcioCastella FROM routes WHERE id=22";
  $descripcioSentencia22 = "descripcioCastella";
  $bicicletesNostres = "Nuestras bicicletas";
  $bicis1 = "Tenemos bicicletas pera todas las edades. Tenemos bicicletas electricas y bicicletas tradicionales. En el momento de alquilar un tour hay la posibilidad de escojer bicicleta.";
  $bicis2 = "Registrate y disfruta de tu tour preferido con toda la família y amigos.";
  $bicicletaTradicional = "Bicicleta tradicional";
  $bicicletaElectrica = "Bicicleta eléctrica";
  $normes = "NORMAS";
  $descrip = "descripcioCastella";
  $titolBlog = "Blog de la empresa";
  $titol = "titolCastella";
  $formulariEnviar = "Enviar";
  $escriuAqui = "Escríbenos aquí...";
  $missatge = "Mensaje de contacto";
  $email = "Correo electrónico";
  $noms = "Nombre y apellidos";
  $msg = "Envianos el problema y te contestaremos lo más antes posible.";

  // INICIAR SESSIO

  $contrasenya = "Contraseña";
  $entrar = "Entrar";
  $oblidacio = "¿No recuerdas la contraseña? Entra";
  $aqui = "aquí";

  // FORM OBLIDAR

  $Recuperarcontrasenya = "Recuperar contraseña";
  $canviarContrasenya = "Cambiar contraseña";
  $tornarIniciSessio = "Volver a iniciar sesión";

  // BICICLETES JA RESERVADES

  $reservaFeta = "Reserva hecha correctamente";
  $tornar = "Volver";

  // USUARI

  $dataMal = "Fecha mal seleccionada";
  $marxar = "Cerrar";
  $eliminarCompte = "Eliminar cuenta";
  $titol_dadesUsuaris = "DATOS DE CONTACTO";
  $editarPwd = "Editar contraseña";
  $bicisReservades = "Bicicletas reservadas";
  $dataReserva = "Fecha de la reserva";
  $ruta = "Ruta";
  $titolReservar = "RESERVAR BICICLETAS";
  $txt = "Indica el dia y hora para reservar las bicicletas. Nos pondremos en contacto.";
  $bicisAReservar = "Bicicletas a reservar";
  $tipus = "Tipo";
  $opcioTriar = "Escoger una opción...";
  $blogEmpresa = "BLOG DE LA EMPRESA";
  $blogMissatgeEnviat = "Mensaje enviado.";
  $bicisJaReservades = "Núm de bicicletas:";
  $comentariBlog = "Escribe un comentario referente a la empresa. Tu comentario puede ayudarnos. <br> Los mensajes se publican al apartado 'Blog' de la pagina principal.";

  // REGISTRAR

  $nom = "Nombre";
  $cognoms = "Apellidos";
  $sexe = "Sexo";
  $ciutat = "Ciudad";
  $pais = "País";
  $DNI = "DNI/Pasaporte";
  $mobil = "Móvil";
  $naixement = "Fecha de nacimiento";
  $targeta = "Targeta bancaria";
  $tornarPagina = "Volver a la página";
  $benvingut = "¡Bienvenido";
  $benvinguda = "¡Bienvenida";

  // FOOTER TEMPS

  function temps(){
    ?>
      <div id="c_52b1ec94b1d29bb00c626b8e56ef5291" class="normal"></div><script type="text/javascript" src="https://www.eltiempo.es/widget/widget_loader/52b1ec94b1d29bb00c626b8e56ef5291"></script>
    <?php
  }
  $paginaUsuari = "Pagina de usuario";

?>
