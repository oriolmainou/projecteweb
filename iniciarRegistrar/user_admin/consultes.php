<?php
	session_start();

	if (!isset($_SESSION['usuario'])) {
		header('Location: ../loginAdmin.php');
	}

	if (isset($_POST['button'])) {
		session_destroy();
		header('Location: ../../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Bike Tour Barcelona</title>
  <meta charset="utf-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body id="top" style="margin-left: 25px; margin-top: 10px; margin-right: 25px">
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
	    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
				<li class="nav-item active">
	        <a class="nav-link" href="principal.php">Inici</a>
	      </li>
				<li class="nav-item active">
	        <b><u><a class="nav-link">CONSULTES</a></b></u>
	      </li>
				<li class="nav-item active">
					<a class="nav-link" href="updateBicis.php">Update bicicleta</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="reserves.php">Reserves</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="administradors.php">Administradors</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="blog.php">Blog</a>
				</li>
	    </ul>
			<span class="navbar-text">
				<form method="POST">
					<button type="submit" name="button" id="buttonMarxar" class="btn btn-danger">Log out</button>
				</form>
			</span>
	  </div>
	</nav>

	<br><br>

	<center>
		<h3>Consultes dels usuaris</h3>
	</center>

  <?php
    $idError = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      if (empty($_POST["eliminarFila"])) {
        $idError = "ID is required";
      } else {
        $usuari2 = "root";
        $contrasenya2 = "";
        $servidor2 = "localhost";
        $basededades2 = "biketourbarcelona";

        $conexion2 = mysqli_connect($servidor2, $usuari2, "") or die ("No se ha conectado");
        $db2 = mysqli_select_db($conexion2, $basededades2) or die ("No se ha conectado");

        $consulta2 = "DELETE FROM contacte WHERE id = " . $_POST['eliminarFila'];
        $resultado2 = mysqli_query($conexion2, $consulta2) or die ("No se ha hecho la consulta");
        mysqli_close($conexion2);
      }
    }

    $usuari = "root";
    $contrasenya = "";
    $servidor = "localhost";
    $basededades = "biketourbarcelona";

    $conexion = mysqli_connect($servidor, $usuari, "") or die ("No se ha conectado");
    $db = mysqli_select_db($conexion, $basededades) or die ("No se ha conectado");
    $consulta = "SELECT * FROM contacte";
    $resultado = mysqli_query($conexion, $consulta) or die ("No se ha hecho la consulta");

    echo "<table borde='2' style='width:100%' bgcolor='#E3F0FD'>";
    echo "<tr>";
    echo "<th><u>ID MISSATGE</u></th>";
    echo "<th><u>NOM I COGNOMS</u></th>";
    echo "<th><u>CORREU ELECTRONIC</u></th>";
    echo "<th><u>MISSATGE DE CONTACTE</u></th>";
    echo "</tr>";

    while ($columna = mysqli_fetch_array($resultado)){
      echo "<tr>";
      echo "<td>" . $columna['id'] . "</td><td>" . $columna['nomCognoms'] . "</td><td>" . $columna['email'] . "</td><td>" . $columna['missatge'] . "</td>";
      echo "</tr>";
    }

    echo "</table>";

  ?>

  <br>

  <form method="post" action="consultes.php">
    <input type="number" name="eliminarFila" placeholder="ID de la fila a eliminar...">
    <button type="submit" value="botonElimina" class="btn btn-info">Eliminar</button>
  </form>

	<br><hr><br>

	<footer class="page-footer font-small">
	  <div class="footer-copyright text-center py-3">Pàgina administrador:
	    <a href="http://localhost:8080/M12-Projecte-Oriol_Mainou/Projecte-Web/"> Bike Tour Barcelona</a>
	  </div>
	</footer>

</body>
</html>
