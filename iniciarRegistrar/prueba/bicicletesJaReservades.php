<?php
	session_start();
	require '../../idioma/requirelanguage.php'; // idioma
	if (!isset($_SESSION['usuario'])) {
		header('Location: ../iniciar.php');
	}

	if (isset($_POST['button'])) {
		session_destroy();
		header('Location: ../../index.php');
	}

?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <title>Bike Tour Barcelona</title>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <link rel="stylesheet" type="text/css" href="css/css.css">
</head>
<body style="background-color: #DDE3F7;">
  <br>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-6">
        <div class="card">
          <article class="card-body">
            <br>
            <center>
              <h3><?php echo $reservaFeta; ?>.</h3>
            </center>
          </article>
          <br>
          <div class="border-top card-body text-center">
            <a href="principal.php"><?php echo $tornar; ?></a>
          </div>
        </div>
        <br>
      </div>
    </div>
  </div>
</body>
</html>
