<?php

  if (empty($_SESSION["language"])) {
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    $_SESSION["language"] = $lang;

    if ($lang != "es") {
      $_SESSION["language"] = "en";
    }
  }

  if (isset($_SESSION["language"])) {
    $lang = $_SESSION["language"];
  }

  switch ($lang) {
    case 'ca':
      include("language/ca.php");
    break;

    case 'es':
      include("language/es.php");
    break;

    case 'en':
      include("language/en.php");
    break;

    default:
      include("language/es.php");
    break;
  }

?>
