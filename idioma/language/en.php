<?php
  $iniciarSessió = "Log in";
  $registrar = "Sign up";
  $catala = "Català";
  $castella = "Castellano";
  $angles = "English";
  $idioma = "Language";
  $menu1 = "Home";
  $menu2 = "Rutes";
  $menu3 = "Bikes";
  $menu4 = "Norms";
  $menu5 = "Blog";
  $menu6 = "Faqs";
  $menu7 = "Contact";
  $descripcio = "We are the leading bike rental service in Barcelona with both electric and traditional bicycles. You can get to know the city with our routes and pay for hours of use.";
  $rutesPrincipalsTitol = "Our main routes";
  $veureRuta= "Click to see the route";
  $sqlSentencia1 = "SELECT descripcioAngles FROM routes WHERE id=1";
  $descripcioSentencia = "descripcioAngles";
  $sqlSentenciaPreu1 = "SELECT preus FROM routes WHERE id=1";
  $preu = "Price";
  $persona = "Person";
  $sqlSentencia2 = "SELECT descripcioAngles FROM routes WHERE id=2";
  $descripcioSentencia2 = "descripcioAngles";
  $sqlSentencia3 = "SELECT descripcioAngles FROM routes WHERE id=3";
  $descripcioSentencia3 = "descripcioAngles";
  $titolUbicacio = "Where are we located?";
  $descripcioUbicacio = "We are located in Barri de Gràcia of Barcelona. The nearest metro lines are: L3 (Lesseps and Fontana) and L4 (Joanic), and wide bus lines (22, 24, 27, 28, 31, 32, 39, 55, 74, 87, 92, 114, 116).";
  $contacteAmbNosaltres = "Contact us";
  $xarxesSocials = "Social networks";
  $sqlSentencia4 = "SELECT descripcioAngles FROM routes WHERE id=4";
  $descripcioSentencia4 = "descripcioAngles";
  $sqlSentencia5 = "SELECT descripcioAngles FROM routes WHERE id=5";
  $descripcioSentencia5 = "descripcioAngles";
  $sqlSentencia6 = "SELECT descripcioAngles FROM routes WHERE id=6";
  $descripcioSentencia6 = "descripcioAngles";
  $sqlSentencia7 = "SELECT descripcioAngles FROM routes WHERE id=7";
  $descripcioSentencia7 = "descripcioAngles";
  $sqlSentencia8 = "SELECT descripcioAngles FROM routes WHERE id=8";
  $descripcioSentencia8 = "descripcioAngles";
  $lesNostresRutes = "Our rutes";
  $sqlSentencia9 = "SELECT descripcioAngles FROM routes WHERE id=9";
  $descripcioSentencia9 = "descripcioAngles";
  $sqlSentencia10 = "SELECT descripcioAngles FROM routes WHERE id=10";
  $descripcioSentencia10 = "descripcioAngles";
  $sqlSentencia11 = "SELECT descripcioAngles FROM routes WHERE id=11";
  $descripcioSentencia11 = "descripcioAngles";
  $seguent = "Next";
  $anterior = "Last";
  $sqlSentencia12 = "SELECT descripcioAngles FROM routes WHERE id=12";
  $descripcioSentencia12 = "descripcioAngles";
  $sqlSentencia13 = "SELECT descripcioAngles FROM routes WHERE id=13";
  $descripcioSentencia13 = "descripcioAngles";
  $sqlSentencia14 = "SELECT descripcioAngles FROM routes WHERE id=14";
  $descripcioSentencia14 = "descripcioAngles";
  $sqlSentencia15 = "SELECT descripcioAngles FROM routes WHERE id=15";
  $descripcioSentencia15 = "descripcioAngles";
  $sqlSentencia16 = "SELECT descripcioAngles FROM routes WHERE id=16";
  $descripcioSentencia16 = "descripcioAngles";
  $sqlSentencia17 = "SELECT descripcioAngles FROM routes WHERE id=17";
  $descripcioSentencia17 = "descripcioAngles";
  $sqlSentencia18 = "SELECT descripcioAngles FROM routes WHERE id=18";
  $descripcioSentencia18 = "descripcioAngles";
  $sqlSentencia19 = "SELECT descripcioAngles FROM routes WHERE id=19";
  $descripcioSentencia19 = "descripcioAngles";
  $sqlSentencia20 = "SELECT descripcioAngles FROM routes WHERE id=20";
  $descripcioSentencia20 = "descripcioAngles";
  $sqlSentencia21 = "SELECT descripcioAngles FROM routes WHERE id=21";
  $descripcioSentencia21 = "descripcioAngles";
  $sqlSentencia22 = "SELECT descripcioAngles FROM routes WHERE id=22";
  $descripcioSentencia22 = "descripcioAngles";
  $bicicletesNostres = "Our bikes";
  $bicis1 = "We have bicycles for all ages. We have electric bicycles and traditional bicycles. At the time of renting a tour, there is the possibility of choosing a bicycle.";
  $bicis2 = "Register and you enjoy your favorite tour with all the family and friends.";
  $bicicletaTradicional = "Traditional bicycle";
  $bicicletaElectrica = "Electric bicycle";
  $normes = "NORMS";
  $descrip = "descripcioAngles";
  $titolBlog = "Blog";
  $titol = "titolAngles";
  $formulariEnviar = "Submit";
  $escriuAqui = "Write to us here...";
  $missatge = "Contact message";
  $email = "Email";
  $noms = "Name and surname";
  $msg = "You send us the problem and we will answer you as soon as possible.";

  // INICIAR SESSIO

  $contrasenya = "Password";
  $entrar = "Submit";
  $oblidacio = "Don't you remember the password? Enter";
  $aqui = "here";

  // FORM OBLIDAR

  $Recuperarcontrasenya = "Recover password";
  $canviarContrasenya = "Change password";
  $tornarIniciSessio = "Return to login";

  // BICICLETES JA RESERVADES

  $reservaFeta = "Booking made correctly";
  $tornar = "Return";

  // USUARI

  $dataMal = "Wrong date";
  $marxar = "Logout";
  $eliminarCompte = "Delete account";
  $titol_dadesUsuaris = "CONTACT INFORMATION";
  $editarPwd = "Edit password";
  $bicisReservades = "Bikes reserved";
  $dataReserva = "Date of reservation";
  $ruta = "Rutes";
  $titolReservar = "BOOK BICYCLES";
  $txt = "Indicate the day and time to reserve bicycles.. We will contact you.";
  $bicisAReservar = "Bicycles to reserve";
  $tipus = "Type";
  $opcioTriar = "Choose an option...";
  $blogEmpresa = "COMPANY BLOG";
  $blogMissatgeEnviat = "Sent message.";
  $bicisJaReservades = "Number of bicycles:";
  $comentariBlog = "Write a comment about the company. Your comment can help us. <br> The messages are published in the 'Blog' section of the home page.";

  // REGISTRAR

  $nom = "Name";
  $cognoms = "Surnames";
  $sexe = "Gender";
  $ciutat = "City";
  $pais = "Country";
  $DNI = "DNI/Passport";
  $mobil = "Mobile phone";
  $naixement = "Birthdate";
  $targeta = "Credit card";
  $tornarPagina = "Return to the page";
  $benvingut = "Welcome";
  $benvinguda = "Welcome";

  // FOOTER TEMPS

  function temps(){
    ?>
      <div id="c_98c6aa9fcd882900025f4174085c9213" class="normal"></div><script type="text/javascript" src="https://en.eltiempo.es/widget/widget_loader/98c6aa9fcd882900025f4174085c9213"></script>
    <?php
  }
  $paginaUsuari = "User page";

?>
