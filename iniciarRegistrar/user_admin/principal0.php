<?php
	session_start();

	if (!isset($_SESSION['usuario'])) {
		header('Location: ../loginAdmin.php');
	}

	if (isset($_POST['button'])) {
		session_destroy();
		header('Location: ../../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Bike Tour Barcelona</title>
  <meta charset="utf-8">
  <link href="../../layout/styles/layout.css" rel="stylesheet" type="text/css" media="all">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<style>
		#buttonMarxar{
			background-color: #008CBA;
			border: black;
			color: white;
			cursor: pointer;
			width: 80px;
			height: 20px;
		}
		.open-button {
		  background-color: #555;
		  color: white;
		  padding: 16px;
		  border: none;
		  cursor: pointer;
		  width: 180px;
		}
		#afegir{
			 padding: 5px;
			 font-weight: 50px;
			 font-size: 10px;
			 color: #ffffff;
			 background-color: #1883ba;
		}
		.open-buttons {
		  background-color: #D67878;
		  color: black;
		  padding: 16px;
		  border: none;
		  cursor: pointer;
		  width: 180px;
		}

		.form-popup {
		  display: none;
		  position: fixed;
		  bottom: 0;
		  right: 15px;
		  border: 3px solid #f1f1f1;
		  z-index: 9;
		}

		.form-container {
		  max-width: 300px;
		  padding: 10px;
		  background-color: white;
		}

		.form-container input[type=text], .form-container input[type=password] {
		  width: 100%;
		  padding: 15px;
		  margin: 5px 0 22px 0;
		  border: none;
		}

		.form-container .btn {
		  background-color: #4CAF50;
		  color: white;
		  border: none;
		  cursor: pointer;
		  margin-bottom:10px;

		}

		.form-container .cancel {
		  background-color: red;
		}
	</style>
</head>
<body id="top">
  <div style="background-color: #F8C471;" class="wrapper row0">
    <div id="topbar" class="hoc clear"><br>
      <div>
        <ul class="nospace inline pushright">
          <li style="float: right;">
						<i class="fa fa-sign-in"></i>
						<form method="POST">
							<button type="submit" id="buttonMarxar" name="button">Marxar</button>
						</form>
						<br>
					</li>
        </ul>
      </div>
    </div>
		<br>
  </div>
	<div class="wrapper row3">
	  <main class="hoc container clear"><center>
			<p style="font-size: 30px;"><b>Compte administrador</p></b><br>
			<center><button class="open-button" onclick="openForm()">Crear administrador</button></center><br>

			<div class="form-popup" id="myForm">
				<form method="post" action="principal.php" class="form-container">
					<label for="email"><b>Email</b></label>
					<input type="email" placeholder="Email" name="email">
					<label for="psw"><b>Password</b></label>
					<input type="password" placeholder="Contrasenya" name="psw">

					<button type="submit" name="crearAdmin" class="btn">Crear</button>
					<button type="button" class="btn cancel" onclick="closeForm()">Tancar</button>
				</form>
				<?php

					if (isset($_POST["crearAdmin"])) {
						$con = mysqli_connect("localhost", "root", "", "biketourbarcelona");
						$email = $_POST['email'];
						$encript = md5($_POST["psw"]);
						mysqli_query($con,"INSERT INTO administrador(nom,contrasenya) VALUES('$email', '$encript')");
						mysqli_close($con);
					}
				?>
			</div>

			<div class="container">
			  <ul class="nav nav-tabs">
			    <li class="active"><a data-toggle="tab" href="#home">Usuars registrats</a></li>
			    <li><a data-toggle="tab" href="#menu1">Consultes dels usuaris</a></li>
					<li><a data-toggle="tab" href="#menuUpdate1">Update bicicletes</a></li>
					<li><a data-toggle="tab" href="#menuReserva">Reserves</a></li>
					<li><a data-toggle="tab" href="#menuAdmins">Administradors</a></li>
					<li><a data-toggle="tab" href="#menuAfegirFitxers">Fitxers rutes</a></li>
			  </ul>

				<div class="tab-content">
			    <div id="home" class="tab-pane fade in active"><br>
						<?php
							$idError = "";
				      if ($_SERVER["REQUEST_METHOD"] == "POST") {
				        if (empty($_POST["eliminarUsuari"])) {
				          $idError = "ID is required";
				        } else {
				          $usuari2 = "root";
				          $contrasenya2 = "";
				          $servidor2 = "localhost";
				          $basededades2 = "biketourbarcelona";

				          $conexion2 = mysqli_connect($servidor2, $usuari2, "") or die ("No se ha conectado");
				          $db2 = mysqli_select_db($conexion2, $basededades2) or die ("No se ha conectado");

				          $consulta2 = "DELETE FROM persona WHERE id_persona = " . $_POST['eliminarUsuari'];
				          $resultado2 = mysqli_query($conexion2, $consulta2) or die ("No se ha hecho la consulta");
									//var_dump($consulta2);
				          mysqli_close($conexion2);
				        }
				      }

				      $usuari = "root";
				      $contrasenya = "";
				      $servidor = "localhost";
				      $basededades = "biketourbarcelona";

				      $conexion = mysqli_connect($servidor, $usuari, "") or die ("No se ha conectado");
				      $db = mysqli_select_db($conexion, $basededades) or die ("No se ha conectado");
				      $consulta = "SELECT * FROM persona";
				      $resultado = mysqli_query($conexion, $consulta) or die ("No se ha hecho la consulta");

							$consultaTreballador = "SELECT * FROM treballador";
				      $resultadoTreballador = mysqli_query($conexion, $consultaTreballador) or die ("No se ha hecho la consulta");

				      echo "<table borde='2' style='width:100%' bgcolor='#E3F0FD'>";
				      echo "<tr>";
				      echo "<th><u>ID PERSONA</u></th>";
				      echo "<th><u>NOM / COGNOMS</u></th>";
							echo "<th><u>DNI</u></th>";
				      echo "<th><u>CORREU ELECTRONIC</u></th>";
							echo "<th><u>MOBIL</u></th>";
				      echo "</tr>";

				      while ($columna = mysqli_fetch_array($resultado)){
				      	echo "<tr>";
				      	echo "<td>" . $columna['id_persona'] . "</td><td>" . $columna['nom'] . " " . $columna['cognoms'] . "</td><td>" . $columna['dni'] . "</td><td>" . $columna['email'] . "</td><td>" . $columna['mobil'] . "</td>";
				        echo "</tr>";
				      }

				      echo "</table>";

							echo "<table borde='2' style='width:100%' bgcolor='#E3F0FD'>";
				      echo "<tr>";
				      echo "<th><u>ID TREBALLADOR</u></th>";
				      echo "<th><u>COMPTE BANCARI</u></th>";
							echo "<th><u>SEGURETAT SOCIAL</u></th>";
				      echo "<th><u>SALARI</u></th>";
				      echo "</tr>";

				      while ($columna = mysqli_fetch_array($resultadoTreballador)){
				      	echo "<tr>";
				      	echo "<td>" . $columna['id_treballador'] . "</td><td>" . $columna['compteBancari'] . "</td><td>" . $columna['seguretatSocial'] . "</td><td>" . $columna['salari'] . "</td>";
								echo "</tr>";
				      }

				      echo "</table>";
				      mysqli_close($conexion);
				    ?>
						<br>
				    <form method="post">
				      <input type="number" name="eliminarUsuari" placeholder="ID de la fila a eliminar..."><br>
				      <button type="submit" value="botonElimina">Eliminar</button><br>
				      <p><?php echo $idError; ?></p>
			    </div>

					<!-- SEGON MENU -->

					<div id="menu1" class="tab-pane fade"><br>
						<?php
							$idError = "";
				      if ($_SERVER["REQUEST_METHOD"] == "POST") {
				        if (empty($_POST["eliminarFila"])) {
				          $idError = "ID is required";
				        } else {
				          $usuari2 = "root";
				          $contrasenya2 = "";
				          $servidor2 = "localhost";
				          $basededades2 = "biketourbarcelona";

				          $conexion2 = mysqli_connect($servidor2, $usuari2, "") or die ("No se ha conectado");
				          $db2 = mysqli_select_db($conexion2, $basededades2) or die ("No se ha conectado");

				          $consulta2 = "DELETE FROM contacte WHERE id = " . $_POST['eliminarFila'];
				          $resultado2 = mysqli_query($conexion2, $consulta2) or die ("No se ha hecho la consulta");
				          mysqli_close($conexion2);
				        }
				      }

				      $usuari = "root";
				      $contrasenya = "";
				      $servidor = "localhost";
				      $basededades = "biketourbarcelona";

				      $conexion = mysqli_connect($servidor, $usuari, "") or die ("No se ha conectado");
				      $db = mysqli_select_db($conexion, $basededades) or die ("No se ha conectado");
				      $consulta = "SELECT * FROM contacte";
				      $resultado = mysqli_query($conexion, $consulta) or die ("No se ha hecho la consulta");

				      echo "<table borde='2' style='width:100%' bgcolor='#E3F0FD'>";
				      echo "<tr>";
				      echo "<th><u>ID MISSATGE</u></th>";
				      echo "<th><u>NOM I COGNOMS</u></th>";
				      echo "<th><u>CORREU ELECTRONIC</u></th>";
				      echo "<th><u>MISSATGE DE CONTACTE</u></th>";
				      echo "</tr>";

				      while ($columna = mysqli_fetch_array($resultado)){
				      	echo "<tr>";
				      	echo "<td>" . $columna['id'] . "</td><td>" . $columna['nomCognoms'] . "</td><td>" . $columna['email'] . "</td><td>" . $columna['missatge'] . "</td>";
				        echo "</tr>";
				      }

				      echo "</table>";

				    ?>
				    <br>
				    <form method="post">
				      <input type="number" name="eliminarFila" placeholder="ID de la fila a eliminar..."><br>
				      <button type="submit" value="botonElimina">Eliminar</button><br>
				      <p><?php echo $idError; ?></p>
			    </div>

					<!-- Tercer menu -->

					<div id="menuUpdate1" class="tab-pane fade"><br><br>
						<form action="user_admin.php" method="post">
							<p>- Quantes bicicletes s'han tornat? </p>
							<input type="number" name="bicicletesTornar"><br>
							<input style="color: black; background-color: #B9B9B9;" type="submit" name="submites" value="Submit">
						</form>
						<?php
							if (isset($_POST["submites"])) {
								$conexiones = mysqli_connect("localhost", "root", "", "biketourbarcelona");

								$bicicletesTornades = $_POST['bicicletesTornar'];

								$reserved = mysqli_query($conexiones,"SELECT reservades from bicicletes ORDER BY id_bicicleta DESC");
								$dispo = mysqli_query($conexiones,"SELECT disponibles from bicicletes ORDER BY id_bicicleta DESC");

								$valor1 = $reserved+$bicicletesTornades;
								$valor2 = $bicicletesTornades-$dispo;

		          	mysqli_query($conexiones,"INSERT INTO bicicletes(reservades,disponibles) VALUES($valor1,$valor2)");
								mysqli_close($conexiones);

							}
						?>
					</div>

					<!-- Quart menu -->

					<div id="menuReserva" class="tab-pane fade"><br>
						<?php
							$idError = "";
				      if ($_SERVER["REQUEST_METHOD"] == "POST") {
				        if (empty($_POST["eliminarFila"])) {
				          $idError = "ID is required";
				        } else {
				          $usuari4 = "root";
				          $contrasenya4 = "";
				          $servidor4 = "localhost";
				          $basededades4 = "biketourbarcelona";

				          $conexion4 = mysqli_connect($servidor4, $usuari4, "") or die ("No se ha conectado");
				          $db4 = mysqli_select_db($conexion4 , $basededades4) or die ("No se ha conectado");

				          $consulta4 = "DELETE FROM bicicletes WHERE id = " . $_POST['eliminarFila'];
				          $resultado4 = mysqli_query($conexion4, $consulta4) or die ("No se ha hecho la consulta");
				          mysqli_close($conexion4);
				        }
				      }

				      $usuari = "root";
				      $contrasenya = "";
				      $servidor = "localhost";
				      $basededades = "biketourbarcelona";

				      $conexion = mysqli_connect($servidor, $usuari, "") or die ("No se ha conectado");
				      $db = mysqli_select_db($conexion, $basededades) or die ("No se ha conectado");
				      $consulta = "SELECT * FROM bicicletes";
				      $resultado = mysqli_query($conexion, $consulta) or die ("No se ha hecho la consulta");

				      echo "<table borde='2' style='width:100%' bgcolor='#E3F0FD'>";
				      echo "<tr>";
				      echo "<th><u>ID</u></th>";
							echo "<th><u>EMAIL</u></th>";
				      echo "<th><u>RESERVADES</u></th>";
							echo "<th><u>DIA/HORA</u></th>";
							echo "<th><u>RUTA</u></th>";

				      echo "</tr>";

				      while ($columna = mysqli_fetch_array($resultado)){
				      	echo "<tr>";
				      	echo "<td>" . $columna['id_bicicleta'] . "</td><td>" . $columna['emailUsuariRegistrat'] . "</td><td>" . $columna['reservades'] . "</td><td>" . $columna['diaHoraReserva'] . "</td><td>" . $columna['ruta'] . "</td>";
				        echo "</tr>";
				      }

				      echo "</table>";
				    ?>
				    <br>

						<br><br>
				    <form method="post">
				      <input type="number" name="eliminarFila" placeholder="ID de la fila a eliminar..."><br>
				      <button type="submit" value="botonElimina">Eliminar</button><br>
				      <p><?php echo $idError; ?></p>
						</form>

						<br><br>

						<form method="post" class="form-container">
							<label for="id"><b>ID a modificar</b></label>
							<input type="number" name="id"><br>

							<label for="bicisReservades"><b>Bicicletes reservades</b></label>
							<input type="number" name="bicisReservades"><br>

							<label for="diaHora"><b>Dia i hora</b></label>
							<input type="datetime-local" name="diaHora"><br>

							<button type="submit" name="actualizar" class="btn">Actualitzar</button>
						</form>
						<?php

							if (isset($_POST["actualizar"])) {

									$usuari2 = "root";
									$contrasenya2 = "";
									$servidor2 = "localhost";
									$basededades2 = "biketourbarcelona";
									$conexion2 = mysqli_connect($servidor2, $usuari2, "") or die ("No se ha conectado");
									$db2 = mysqli_select_db($conexion2, $basededades2) or die ("No se ha conectado");

									$encript = md5($_POST["psw"]);


									$consultas = "SELECT * FROM bicicletes  WHERE id_bicicleta='" .
									 $_POST["id"] . "'";
									$resultados = mysqli_query($conexions, $consultas) or die ("No se ha hecho la consulta");
									var_dump($resultados);
									echo "<br>";
									$todo = $resultados["disponibles"]+$resultados["reservades"];
									var_dump($todo);
									echo "<br>";

									/*while ($row = $resultados->fetch()) {
										$disponibles = $row['disponibles'];
									}*/

									$disponible = $todo - $_POST['bicisReservades'];
									var_dump($disponible);
									echo "<br>";

									$consulta1 = "UPDATE bicicletes SET disponibles=$disponible,
									 diaHoraReserva='" . $_POST["diaHora"] .
									 "', reservades='" . $_POST["bicisReservades"] . "'  WHERE id_bicicleta='" .
										$_POST["id"] . "'";
										var_dump($consulta1);
										echo "<br>";
										die;
									$returns = mysqli_query($conexion2,$consulta1);

									// $consultar = "UPDATE bicicletes SET disponibles=$disponible";
									// $returns = mysqli_query($conexion2,$consultar);
							}
						?>
					</div>

					<!-- 5è menu -->

					<div id="menuAdmins" class="tab-pane fade"><br>
						<?php
							$usuaris = "root";
							$contrasenyes = "";
							$servidors = "localhost";
							$basededadess = "biketourbarcelona";

							$conexions = mysqli_connect($servidors, $usuaris, "") or die ("No se ha conectado");
							$dbs = mysqli_select_db($conexions, $basededadess) or die ("No se ha conectado");
							$consultas = "SELECT * FROM administrador";
							$resultados = mysqli_query($conexions, $consultas) or die ("No se ha hecho la consulta");

							echo "<table borde='2' style='width:100%' bgcolor='#E3F0FD'>";
							echo "<tr>";
							echo "<th><u>Email</u></th>";
							echo "</tr>";

							while ($columnas = mysqli_fetch_array($resultados)){
								echo "<tr>";
								echo "<td>" . $columnas['nom'] . "</td>";
								echo "</tr>";
							}

							echo "</table>";
						?>
					</div>

					<!-- 6è menú -->

					<div id="menuAfegirFitxers" class="tab-pane fade"><br>
						<form action="principal.php" method="post">
							Ruta 1:<input type="file" name="Afegir">
							<input type="submit" name="afegir" id="afegir">
						</form>
						<?php
							if (isset($_POST["afegir"])) {
								$usuarios = "root";
								$contrasenas = "";
								$servidores = "localhost";
								$basededatos = "biketourbarcelona";

								$afegir = $_POST["afegir"];

								$conexiones = mysqli_connect($servidores, $usuarios, "") or die ("No se ha conectado");
								$dbss = mysqli_select_db($conexiones, $basededatos) or die ("No se ha conectado");
								$consultes = "UPDATE routes SET foto='$afegir' WHERE id=1";
								$resultats = mysqli_query($conexiones, $consultes) or die ("No se ha hecho la consulta");
							}
						?>
					</div>
			  </div>
			</div>
	  </main>
	</div>
	<div class="wrapper row4 bgded overlay" style="background-color: #FAFAFA;">
		<footer id="footer" class="hoc clear">
			<div class="one_quarter first">
				<h6 class="title">Bike Tour Barcelona</h6>
				<p>Som el servei de lloguer de bicicletes líder de Barcelona amb bicicletes tant elèctiques com tradicionals. Coneix la ciutat amb les nostres rutes i paga per hores d'ús.</p>
			</div>
			<div class="one_quarter">
				<h6 class="title">Contacte amb nosaltres</h6>
				<ul class="nospace linklist contact">
					<li><i class="fa fa-map-marker"></i>
						<address>Plaça del Nord 14 <br>08029 Barcelona</address>
					</li>
					<li><i class="fa fa-phone"></i>+34 934.547.411</li>
					<li><i class="fa fa-envelope-o"></i>biketour@bcnbiketour.cat</li>
				</ul>
			</div>
			<div class="one_quarter">
				<h6 class="title">Xarxes socials</h6>
				<ul class="nospace linklist contact">
					<li><i class="fa fa-twitter-square"></i><a style="color: white" href="#" title="Twitter">Twitter</a></li>
					<li><i class="fa fa-facebook-square"></i><a style="color: white" href="#" title="Facebook">Facebook</a></li>
					<li><i class="fa fa-instagram"></i><a style="color: white" href="#" title="Instagram">Instagram</a></li>
					<a href="#"></a>
				</ul>
			</div>
			<div>
        <div id="c_5b272340ccb31f066336d9e56dd98bfb" class="normal"></div><script type="text/javascript" src="https://ca.eltiempo.es/widget/widget_loader/5b272340ccb31f066336d9e56dd98bfb"></script>
      </div>
		</footer>
	</div>

	<script>
		function openForm() {
			document.getElementById("myForms").style.display = "block";
		}

		function closeForm() {
			document.getElementById("myForms").style.display = "none";
		}
	</script>
	<!-- -->
	<script src="layout/scripts/jquery.min.js"></script>
	<script src="layout/scripts/jquery.backtotop.js"></script>
	<script src="layout/scripts/jquery.mobilemenu.js"></script>
</body>
</html>
