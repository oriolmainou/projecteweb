<?php
	session_start();

	if (!isset($_SESSION['usuario'])) {
		header('Location: ../loginAdmin.php');
	}

	if (isset($_POST['button'])) {
		session_destroy();
		header('Location: ../../index.php');
	}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Bike Tour Barcelona</title>
  <meta charset="utf-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body id="top" style="margin-left: 25px; margin-top: 10px; margin-right: 25px">
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
	    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
				<li class="nav-item active">
	        <b><u><a class="nav-link">INICI</a></b></u>
	      </li>
				<li class="nav-item active">
	        <a class="nav-link" href="consultes.php">Consultes</a>
	      </li>
				<li class="nav-item active">
					<a class="nav-link" href="updateBicis.php">Update bicicleta</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="reserves.php">Reserves</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="administradors.php">Administradors</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="blog.php">Blog</a>
				</li>
	    </ul>
			<span class="navbar-text">
				<form method="POST">
					<button type="submit" name="button" id="buttonMarxar" class="btn btn-danger">Log out</button>
				</form><br>
			</span>
	  </div>
	</nav>

	<br><br>

	<center>
		<h3>Crear administrador</h3>
	</center>

	<form action="principal.php" style="margin-right: 70%" method="post">
		<div class="form-group">
			<label for="email">Correu electrònic:</label>
			<input type="email" placeholder="Correu electrònic" class="form-control" name="email">
		</div>
		<div class="form-group">
			<label for="pwd">Contrasenya:</label>
			<input type="password" class="form-control" name="psw" placeholder="Contrasenya">
		</div>
		<button type="submit" name="crearAdmin" class="btn btn-primary">Crear</button>
	</form><br>

	<?php
		$conn = mysqli_connect("localhost", "root", "", "biketourbarcelona");

		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}

		if (isset($_POST["crearAdmin"])) {
			$correus = $_POST['email'];
			$encript = md5($_POST["psw"]);

			$sql = "INSERT INTO administrador (nom, contrasenya) VALUES ('$correus', '$encript')";

			if (mysqli_query($conn, $sql)) {
				echo "Creat un nou administrador";
			} else {
				echo "Error: " . $sql . "<br>" . mysqli_error($conn);
			}
		}

		mysqli_close($conn);
	?>

	<br><hr><br>

	<center>
		<h3>Els nostres usuaris</h3>
	</center>

	<br>

	<?php
		$idError = "";
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			if (empty($_POST["eliminarUsuari"])) {
				$idError = "ID is required";
			} else {
				$usuari2 = "root";
				$contrasenya2 = "";
				$servidor2 = "localhost";
				$basededades2 = "biketourbarcelona";

				$conexion2 = mysqli_connect($servidor2, $usuari2, "") or die ("No se ha conectado");
				$db2 = mysqli_select_db($conexion2, $basededades2) or die ("No se ha conectado");

				$consulta2 = "DELETE FROM persona WHERE id_persona = " . $_POST['eliminarUsuari'];
				$resultado2 = mysqli_query($conexion2, $consulta2) or die ("No se ha hecho la consulta");
				mysqli_close($conexion2);
			}
		}

		$usuari = "root";
		$contrasenya = "";
		$servidor = "localhost";
		$basededades = "biketourbarcelona";

		$conexion = mysqli_connect($servidor, $usuari, "") or die ("No se ha conectado");
		$db = mysqli_select_db($conexion, $basededades) or die ("No se ha conectado");
		$consulta = "SELECT * FROM persona";
		$resultado = mysqli_query($conexion, $consulta) or die ("No se ha hecho la consulta");

		$consultaTreballador = "SELECT * FROM treballador";
		$resultadoTreballador = mysqli_query($conexion, $consultaTreballador) or die ("No se ha hecho la consulta");

		echo "<table borde='2' style='width:100%' bgcolor='#E3F0FD'>";
		echo "<tr>";
		echo "<th><u>ID PERSONA</u></th>";
		echo "<th><u>NOM / COGNOMS</u></th>";
		echo "<th><u>DNI</u></th>";
		echo "<th><u>CORREU ELECTRONIC</u></th>";
		echo "<th><u>MOBIL</u></th>";
		echo "</tr>";

		while ($columna = mysqli_fetch_array($resultado)){
			echo "<tr>";
			echo "<td>" . $columna['id_persona'] . "</td><td>" . $columna['nom'] . " " . $columna['cognoms'] . "</td><td>" . $columna['dni'] . "</td><td>" . $columna['email'] . "</td><td>" . $columna['mobil'] . "</td>";
			echo "</tr>";
		}

		echo "</table>";

	?>

	<br><br>

	<center>
		<h3>Els nostres treballadors</h3>
	</center>

	<?php
		echo "<table borde='2' style='width:100%' bgcolor='#E3F0FD'>";
		echo "<tr>";
		echo "<th><u>ID TREBALLADOR</u></th>";
		echo "<th><u>COMPTE BANCARI</u></th>";
		echo "<th><u>SEGURETAT SOCIAL</u></th>";
		echo "<th><u>SALARI</u></th>";
		echo "</tr>";

		while ($columna = mysqli_fetch_array($resultadoTreballador)){
			echo "<tr>";
			echo "<td>" . $columna['id_treballador'] . "</td><td>" . $columna['compteBancari'] . "</td><td>" . $columna['seguretatSocial'] . "</td><td>" . $columna['salari'] . "</td>";
			echo "</tr>";
		}

		echo "</table>";
		mysqli_close($conexion);
	?>

	<br>

	<form method="post">
		<input type="number" name="eliminarUsuari" placeholder="ID a eliminar...">
		<button type="submit" value="botonElimina" class="btn btn-info">Eliminar</button>
	</form><br>

	<br><hr><br><br>

	<center>
		<h3>Imatges de la web</h3>
	</center><br>

	<form action="cargar2.php" method="post" enctype="multipart/form-data">
		ID: <input required type="number" name="idForms"><br><br>
		Imatge: <input required type="file" name="img" id="image" multiple><br><br>
		<input type="submit" name="submit" value="Aceptar" class="btn btn-primary">
  </form>

	<br><hr><br>

	<center>
		<h3>Rutes de la pàgina</h3>
	</center>
	<select>
		<?php
			$conn = new mysqli("localhost", "root", "", "biketourbarcelona");
			$sql = "SELECT titol FROM routes";
			$result = $conn->query($sql);

			if ($result->num_rows > 0) {
				while($row = $result->fetch_assoc()) {
					echo "<option>- " . $row["titol"] . "</option>";
				}
			}

			$conn->close();
		?>
	</select>

	<br><hr><br>

	<footer class="page-footer font-small">
	  <div class="footer-copyright text-center py-3">Pàgina administrador:
	    <a href="http://localhost:8080/M12-Projecte-Oriol_Mainou/Projecte-Web/"> Bike Tour Barcelona</a>
	  </div>
	</footer>

</body>
</html>
