<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Bike Tour Barcelona</title>
    <meta charset="utf-8">
    <style>
      .button {
        background-color: #4CAF50; /* Green */
        border-color: black;
        color: white;
        padding: 10px;
        text-align: center;
        font-size: 15px;
        margin: 4px;
        cursor: pointer;
      }
    </style>
  </head>
  <body style="background-color: #F0E68C;">
    <center>
      <h2><u>Usuaris registrats a la base de dades</u></h2>
    </center>
      <?php
        $usuari = "root";
        $contrasenya = "";
        $servidor = "localhost";
        $basededades = "biketourbarcelona";

        $conexion = mysqli_connect($servidor, $usuari, "") or die ("No se ha conectado");
        $db = mysqli_select_db($conexion, $basededades) or die ("No se ha conectado");
        $consulta = "SELECT * FROM persona";
        $resultado = mysqli_query($conexion, $consulta) or die ("No se ha hecho la consulta");

        echo "<b>Persones:</b><br><br>";
        echo "<table borde='2' style='width:100%' bgcolor='#E3F0FD'>";
        echo "<tr>";
        echo "<th><u>ID</u></th>";
        echo "<th><u>NOM</u></th>";
        echo "<th><u>COGNOMS</u></th>";
        echo "<th><u>DNI</u></th>";
        echo "<th><u>EMAIL</u></th>";
        echo "<th><u>NACIONALITAT</u></th>";
        echo "<th><u>MOBIL</u></th>";
        echo "</tr>";

        while ($columna = mysqli_fetch_array($resultado)){
        	echo "<tr>";
        	echo "<td><b>" . $columna['id_persona'] . "</b></td><td>" . $columna['nom'] . "</td><td>" . $columna['cognoms'] . "</td><td>" . $columna['dni'] . "</td><td>" . $columna['email'] . "</td><td>" . $columna['nacionalitat'] . "</td><td>" . $columna['mobil'] . "</td>";
          echo "</tr>";
        }
        echo "</table>";
        mysqli_close($conexion);

        echo "<br><br><hr><br>";

        $conexion2 = mysqli_connect($servidor, $usuari, "") or die ("No se ha conectado");
        $db2 = mysqli_select_db($conexion2, $basededades) or die ("No se ha conectado");
        $consulta2 = "SELECT * FROM client";
        $resultado2 = mysqli_query($conexion2, $consulta2) or die ("No se ha hecho la consulta");

        echo "<b>Clients:</b><br><br>";
        echo "<table borde='2' style='width:100%' bgcolor='#E3F0FD'>";
        echo "<tr>";
        echo "<th><u>ID</u></th>";
        echo "<th><u>TARGETA</u></th>";
        echo "<th><u>POBLACIÓ</u></th>";
        // echo "<th><u>ELIMINAR</u></th>";
        echo "</tr>";
        ?>
        <form class="" method="post">
        <?php
        while ($columna2 = mysqli_fetch_array($resultado2)){
        	echo "<tr>";
        	echo "<td><b>" . $columna2['id_client'] . "</b></td><td>" . $columna2['numTargeta'] . "</td><td>" . $columna2['poblacio'] . "</td><td>
          <input style='float: center;' type='checkbox' name='checked_id[]' value='.$columna2['id_client'].'></td>";
          echo "</tr>";
        }

        echo "</table>";
        mysqli_close($conexion2);

        if (isset($_POST['eliminarUsuaris'])) {
          $deleteCliente = $_POST['checked_id'];
          foreach ($deleteCliente as $id) {
            mysqli_query($conexion, "DELETE FROM client WHERE id_client = $id");
          }
        }
      ?>
      <br><br>

    <p>Eliminar usuaris: <input type="submit" value="Eliminar" name="eliminarUsuaris"/></p>
    </form>

    <br><br>
    <button type="button" class="button" onclick="location.href='../iniciarRegistrar/user_admin/principal.php'">Tornar pagina admin</button>
  </body>
</html>
